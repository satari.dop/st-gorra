<footer>
      <div class="container">
        
        <div class="row">
          <div class="col-sm-12 col-md-1">
             <div class="footer-section">
              <div class="logo">
                  <img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" />
              </div>
            </div>
          </div>
          <div class="col-sm-12  col-md-4">
            <div class="footer-section">
              <div class="social">
                <p><span class=" col-4 col-sm-4 text-right">ติดต่อได้ที่ : </span><span class="col-8 col-sm-8 text-left"><?php echo Modules::run('banner/social') ?></span></p>
                <p><span class=" col-4 col-sm-4 text-right">โทร : </span><span class="col-8 col-sm-8 text-left"><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a></span></p>
                <p><a href="mailto:<?php echo $mailDefault->value; ?>" target="_self"><span class=" col-4 col-sm-4 text-right">อีเมล : </span><span class="col-8 col-sm-8 text-left"><?php echo $mailDefault->value; ?></span></a></p>
             
              </div> 
             
            </div><!-- footer-section -->
          </div><!-- col-lg-4 col-md-6 -->

          <div class="col-12 col-sm-12 col-md-2">
              <div class="footer-section">
               
                 <ul class="main-menu visible-on-click" id="main-menu">
                      <li class="<?php if(!empty($home_act)){ echo $home_act; } ?> "><a href="<?php echo site_url() ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-compass.svg" alt="Logo Image" >อ.กอร่า</a></li>
                      <li class="<?php if(!empty($article_act)){ echo $article_act; } ?>"><a href="<?php echo site_url('article') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-text.svg" alt="Logo Image" >บทความกราฟฟิก</a></li>
                      <li class="drop-down <?php if(!empty($course_act)){ echo $course_act; } ?>"><a href="javascript:void(0)"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pen.svg" alt="Logo Image" >คอร์สเรียนออนไลน์  <i class="ion-ios-arrow-forward">

                      </i></a>

                    

                      </li>
                      
                      <li class="<?php if(!empty($activity_act)){ echo $activity_act; } ?> "><a href="<?php echo site_url('activity') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pencil.svg" alt="Logo Image" >สอนสด</a></li>
                      <li><a href="<?php echo site_url('contact') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-contactus.svg" alt="Logo Image" >ติดต่อเรา</a></li>
                  </ul><!-- main-menu -->
                    
              </div><!-- footer-section -->
          </div>
          <div class="col-12 col-sm-12 col-md-2">
            <div class="footer-section">
             
               <ul class="main-menu main-menu2 visible-on-click" id="main-menu">
                    

                    <?php echo Modules::run('course/top_menu') ?>

                  
                </ul><!-- main-menu -->
                  
            </div><!-- footer-section -->
          </div>
          
          <div class="col-12 col-sm-12 col-md-2">
            <div class="footer-section">
             
               <ul class="main-menu main-menu2 visible-on-click" id="main-menu">
                    <?php if ( !$isLogin) : ?>
                    <li>
                    <a href="<?php echo site_url("user/register/") ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M20.31,6A8,8,0,0,1,26,8.38,7.75,7.75,0,0,1,27.68,11a7.93,7.93,0,0,1,0,6.1A7.79,7.79,0,0,1,26,19.67a8.06,8.06,0,0,1-2.59,1.73,8.05,8.05,0,0,1-6.1,0,8.06,8.06,0,0,1-2.59-1.73,7.84,7.84,0,0,1-1.73-2.6,7.91,7.91,0,0,1,0-6.09,7.84,7.84,0,0,1,1.73-2.6A8,8,0,0,1,20.31,6Zm2.23,2.6a5.87,5.87,0,0,0-4.46,0A5.75,5.75,0,0,0,16.19,9.9a5.85,5.85,0,0,0-1.27,6.36,6,6,0,0,0,1.27,1.89,5.75,5.75,0,0,0,1.89,1.26,5.84,5.84,0,0,0,6.35-1.26,5.83,5.83,0,0,0,0-8.25,5.75,5.75,0,0,0-1.89-1.26Zm.95,13.85a11.65,11.65,0,0,1,2.39,1A12.38,12.38,0,0,1,28,25a11.47,11.47,0,0,1,2.29,2.71,10.88,10.88,0,0,1,1.22,3,3.1,3.1,0,0,1-.57,2.69h0a3,3,0,0,1-1.1.89,3.08,3.08,0,0,1-1.38.32H12.17a3.16,3.16,0,0,1-2.48-1.21,3.18,3.18,0,0,1-.6-1.29,3.06,3.06,0,0,1,0-1.41,11.17,11.17,0,0,1,1.21-3A11.47,11.47,0,0,1,12.63,25a12.38,12.38,0,0,1,2.11-1.49,11.65,11.65,0,0,1,2.39-1l.85-.24.4.8,1.93,3.84,1.93-3.84.4-.8.85.24Zm1.36,2.87a9.33,9.33,0,0,0-1.11-.52l-2.47,4.92-1,1.91-1-1.91-2.47-4.92a8.52,8.52,0,0,0-1.1.52,9.24,9.24,0,0,0-1.73,1.21,9,9,0,0,0-1.86,2.2,8.85,8.85,0,0,0-1,2.44,1,1,0,0,0,0,.45,1,1,0,0,0,.2.41.91.91,0,0,0,.34.28.87.87,0,0,0,.44.1H28.45a.89.89,0,0,0,.44-.1.81.81,0,0,0,.34-.28,1,1,0,0,0,.2-.41,1,1,0,0,0,0-.45,8.85,8.85,0,0,0-1-2.44,9,9,0,0,0-1.86-2.2,9.5,9.5,0,0,0-1.72-1.21Z"></path></svg>
                        <span>สมัครสมาชิก</span>
                    </a>
                    </li>
                    <?php else : ?>
                    <li>
                     <a href="<?php echo site_url("user/profile/") ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M20.31,6A8,8,0,0,1,26,8.38,7.75,7.75,0,0,1,27.68,11a7.93,7.93,0,0,1,0,6.1A7.79,7.79,0,0,1,26,19.67a8.06,8.06,0,0,1-2.59,1.73,8.05,8.05,0,0,1-6.1,0,8.06,8.06,0,0,1-2.59-1.73,7.84,7.84,0,0,1-1.73-2.6,7.91,7.91,0,0,1,0-6.09,7.84,7.84,0,0,1,1.73-2.6A8,8,0,0,1,20.31,6Zm2.23,2.6a5.87,5.87,0,0,0-4.46,0A5.75,5.75,0,0,0,16.19,9.9a5.85,5.85,0,0,0-1.27,6.36,6,6,0,0,0,1.27,1.89,5.75,5.75,0,0,0,1.89,1.26,5.84,5.84,0,0,0,6.35-1.26,5.83,5.83,0,0,0,0-8.25,5.75,5.75,0,0,0-1.89-1.26Zm.95,13.85a11.65,11.65,0,0,1,2.39,1A12.38,12.38,0,0,1,28,25a11.47,11.47,0,0,1,2.29,2.71,10.88,10.88,0,0,1,1.22,3,3.1,3.1,0,0,1-.57,2.69h0a3,3,0,0,1-1.1.89,3.08,3.08,0,0,1-1.38.32H12.17a3.16,3.16,0,0,1-2.48-1.21,3.18,3.18,0,0,1-.6-1.29,3.06,3.06,0,0,1,0-1.41,11.17,11.17,0,0,1,1.21-3A11.47,11.47,0,0,1,12.63,25a12.38,12.38,0,0,1,2.11-1.49,11.65,11.65,0,0,1,2.39-1l.85-.24.4.8,1.93,3.84,1.93-3.84.4-.8.85.24Zm1.36,2.87a9.33,9.33,0,0,0-1.11-.52l-2.47,4.92-1,1.91-1-1.91-2.47-4.92a8.52,8.52,0,0,0-1.1.52,9.24,9.24,0,0,0-1.73,1.21,9,9,0,0,0-1.86,2.2,8.85,8.85,0,0,0-1,2.44,1,1,0,0,0,0,.45,1,1,0,0,0,.2.41.91.91,0,0,0,.34.28.87.87,0,0,0,.44.1H28.45a.89.89,0,0,0,.44-.1.81.81,0,0,0,.34-.28,1,1,0,0,0,.2-.41,1,1,0,0,0,0-.45,8.85,8.85,0,0,0-1-2.44,9,9,0,0,0-1.86-2.2,9.5,9.5,0,0,0-1.72-1.21Z"></path></svg>
                        <span><?php echo $this->session->member['name'] ?></span> 
                    </a>
                  </li>
                     <?php endif; ?>
                     <?php if ( !$isLogin) : ?>
                      <li>
                    <a href="#" data-toggle="modal" data-target="#login">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M13.54,5.76H28.43A3.8,3.8,0,0,1,31.1,6.87h0a3.8,3.8,0,0,1,1.11,2.67V30.07a3.77,3.77,0,0,1-3.78,3.77H13.54a3.78,3.78,0,0,1-2.66-1.1h0a3.73,3.73,0,0,1-1.11-2.66V27.68H12v2.39a1.55,1.55,0,0,0,.45,1.1h0a1.54,1.54,0,0,0,1.09.45H27.93l-8.64-2.31a3.31,3.31,0,0,1-1.78-1.19,3.28,3.28,0,0,1-.69-2V8H13.54A1.57,1.57,0,0,0,12,9.54V12.4H9.77V9.54a3.79,3.79,0,0,1,3.77-3.78Zm-2.75,9.73,3.76,3.76.79.79-.79.78-3.76,3.77L9.22,23l1.86-1.87H5.65V18.93h5.43L9.22,17.06l1.57-1.57Zm11.37-.33a1.59,1.59,0,1,1-1.58,1.59,1.59,1.59,0,0,1,1.58-1.59ZM30,29.88V9.54a1.56,1.56,0,0,0-.46-1.1h0A1.56,1.56,0,0,0,28.43,8H19V26.09a1.17,1.17,0,0,0,.23.69,1.11,1.11,0,0,0,.6.39L30,29.88Z"></path></svg>
                        <span>เข้าสู่ระบบ</span>
                    </a>
                  </li>
                    <?php else : ?>
                      <li>
                        <a href="<?php echo site_url("logout") ?>" >
                             <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M13.54,5.76H28.43A3.8,3.8,0,0,1,31.1,6.87h0a3.8,3.8,0,0,1,1.11,2.67V30.07a3.77,3.77,0,0,1-3.78,3.77H13.54a3.78,3.78,0,0,1-2.66-1.1h0a3.73,3.73,0,0,1-1.11-2.66V27.68H12v2.39a1.55,1.55,0,0,0,.45,1.1h0a1.54,1.54,0,0,0,1.09.45H27.93l-8.64-2.31a3.31,3.31,0,0,1-1.78-1.19,3.28,3.28,0,0,1-.69-2V8H13.54A1.57,1.57,0,0,0,12,9.54V12.4H9.77V9.54a3.79,3.79,0,0,1,3.77-3.78Zm-2.75,9.73,3.76,3.76.79.79-.79.78-3.76,3.77L9.22,23l1.86-1.87H5.65V18.93h5.43L9.22,17.06l1.57-1.57Zm11.37-.33a1.59,1.59,0,1,1-1.58,1.59,1.59,1.59,0,0,1,1.58-1.59ZM30,29.88V9.54a1.56,1.56,0,0,0-.46-1.1h0A1.56,1.56,0,0,0,28.43,8H19V26.09a1.17,1.17,0,0,0,.23.69,1.11,1.11,0,0,0,.6.39L30,29.88Z"></path></svg>
                        <span>ออกจากระบบ</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("course/course_history") ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M28.6,27.53a3.88,3.88,0,1,1-2.74,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM10.72,10.68H35.64a.89.89,0,0,1,.89.89V12a.88.88,0,0,1-.89.88H11.31l1.12,4.18H33.65a.89.89,0,0,1,.89.89v.44a.89.89,0,0,1-.89.89H13l.32,1.2A3.28,3.28,0,0,0,16.57,23H32.08a.88.88,0,0,1,.89.88v.45a.89.89,0,0,1-.89.89H16.57a5.52,5.52,0,0,1-5.37-4.12L8.1,9.49A3.28,3.28,0,0,0,4.88,7h-3A.89.89,0,0,1,1,6.14V5.69a.89.89,0,0,1,.89-.89h3A5.58,5.58,0,0,1,8.27,5.94a5.48,5.48,0,0,1,2,3l.48,1.76Zm6.14,16.85a3.88,3.88,0,1,1-2.73,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM18,30.23a1.68,1.68,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48,1.66,1.66,0,0,0-.48,1.17,1.65,1.65,0,1,0,3.3,0A1.66,1.66,0,0,0,18,30.23Zm11.74,0a1.7,1.7,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48A1.66,1.66,0,0,0,27,31.4a1.64,1.64,0,0,0,1.65,1.65,1.66,1.66,0,0,0,1.17-.48,1.64,1.64,0,0,0,.48-1.17,1.66,1.66,0,0,0-.48-1.17Z"></path></svg>
                            <span>รายการสั่งซื้อ</span> 
                        </a>
                      </li>
                    <?php endif; ?>
                </ul><!-- main-menu -->
                  
            </div><!-- footer-section -->
          </div><!-- col-lg-4 col-md-6 -->

      </div><!-- row -->

      <div class="row">
        <div class="col-sm-12 text-center">
          <hr>
          <span class="copyright">© 2018 Gorradesign. All rights reserved.</span>
        </div>
      </div>

    </div><!-- container -->
  </footer>

  <!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/>
       <!--  <h5 class="modal-title" id="exampleModalLabel">เข้าสู่ระบบ</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login-box-body">
            <div class="loginmodal-container">
                <h1>เข้าสู่ระบบ</h1><br>
                <?php
    $form=array('accept-charset'=>'utf-8','class'=>'form-horizontal','id'=>'frm-login','name'=>'frm-login'); 
    echo form_open('/user/login/check', $form);
    echo form_hidden('base_url',base_url());
    echo form_hidden('site_url',site_url());
    
?>
                <input type="text" name="username" id="username_login" placeholder="Username">
                <input type="password" name="password" id="password_login" placeholder="Password">
              <div id="form-success-div_login" class="text-success"></div> 
               <div id="form-error-div_login" class="text-danger"></div>
              <button type="submit" name="login" class="login btn-login" >เข้าสู่ระบบ</button> 

                <a href="<?=$authUrl?>" class="login btn-facebook"><i class="fa fa-facebook"></i> เข้าสู่ระบบด้วย Facebook</a>
                   <?php echo form_close(); ?>

                <div class="login-help">
                <!--<a href="<?php echo site_url("user/register/") ?>">สมัครสมาชิก</a>  - <a href="#">Forgot Password</a> -->
                </div>
            </div>

            

          </div>
                  
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>