<footer>
	<div class="container">

		<div class="row">
			<div class="col-sm-12 col-md-1">
				<div class="footer-section">
					<div class="logo">
						<img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" />
					</div>
				</div>
			</div>
			<div class="col-sm-12  col-md-4">
				<div class="footer-section">
					<div class="social">
						<div class="row">
							<span class="col-md-4 text-right">ติดต่อได้ที่ : </span>
							<span class="col-md-8 text-left not-left"><?php echo Modules::run('banner/social') ?></span>
							<span class="col-md-4 text-right">โทร : </span>
							<span class="col-md-8 text-left not-left"><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a></span>
							<span class="col-md-4 text-right"><a href="mailto:<?php echo $mailDefault->value; ?>" target="_self">อีเมล : </span>
								<span class="text-left"><?php echo $mailDefault->value; ?></a></span>
							</div>
						</div> 

					</div><!-- footer-section -->
				</div><!-- col-lg-4 col-md-6 -->

				<div class="col-12 col-sm-12 col-md-2">
					<div class="footer-section">

						<ul class="main-menu visible-on-click" id="main-menu">
							<li class="<?php if(!empty($home_act)){ echo $home_act; } ?> "><a href="<?php echo site_url() ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-compass.svg" alt="Logo Image" >อ.กอร่า</a></li>
							<li class="<?php if(!empty($article_act)){ echo $article_act; } ?>"><a href="<?php echo site_url('article') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-text.svg" alt="Logo Image" >บทความกราฟฟิก</a></li>
							<li class="drop-down <?php if(!empty($course_act)){ echo $course_act; } ?>"><a href="javascript:void(0)"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pen.svg" alt="Logo Image" >คอร์สเรียนออนไลน์  <i class="ion-ios-arrow-forward sub-icon">

							</i></a>



						</li>

						<li class="<?php if(!empty($activity_act)){ echo $activity_act; } ?> "><a href="<?php echo site_url('activity') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pencil.svg" alt="Logo Image" >สอนสด</a></li>
						<li><a href="<?php echo site_url('contact') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-contactus.svg" alt="Logo Image" >ติดต่อเรา</a></li>
					</ul><!-- main-menu -->

				</div><!-- footer-section -->
			</div>
			<div class="col-12 col-sm-12 col-md-3">
				<div class="footer-section">

					<ul class="main-menu main-menu2 visible-on-click" id="main-menu">


						<?php echo Modules::run('course/top_menu') ?>


					</ul><!-- main-menu -->

				</div><!-- footer-section -->
			</div>

			<div class="col-12 col-sm-12 col-md-2">
				<div class="footer-section">

					<ul class="main-menu main-menu2 visible-on-click" id="main-menu">
						<?php if ( !$isLogin) : ?>
							<li>
								<a href="<?php echo site_url("user/register/") ?>">
									<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-member-w.svg"  />
									<span>สมัครสมาชิก</span>
								</a>
							</li>
							<?php else : ?>
								<li>
									<a href="<?php echo site_url("user/profile/") ?>">
										<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-member-w.svg"  />
										<span><?php echo $this->session->member['name'] ?></span> 
									</a>
								</li>
							<?php endif; ?>
							<?php if ( !$isLogin) : ?>
								<li>
									<a href="#" data-toggle="modal" data-target="#login">
										<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-login-w.svg"  />
										<span>เข้าสู่ระบบ</span>
									</a>
								</li>
								<?php else : ?>
									<li>
										<a href="<?php echo site_url("logout") ?>" >
											<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-login-w.svg"  />
											<span>ออกจากระบบ</span>
										</a>
									</li>
									<li>
										<a href="<?php echo site_url("course/course_history") ?>">
											<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-car-w.svg"  />
											<span>รายการสั่งซื้อ</span> 
										</a>
									</li>
								<?php endif; ?>
							</ul><!-- main-menu -->

						</div><!-- footer-section -->
					</div><!-- col-lg-4 col-md-6 -->

				</div><!-- row -->

				<div class="row">
					<div class="col-sm-12 text-center">
						<hr>
						<span class="copyright">© 2018 Gorradesign. All rights reserved.</span>
					</div>
				</div>

			</div><!-- container -->
		</footer>

		<!-- Modal -->
		<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/>
						<!--  <h5 class="modal-title" id="exampleModalLabel">เข้าสู่ระบบ</h5> -->
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="login-box-body">
							<div class="loginmodal-container" id="loginForm">
								<h1>เข้าสู่ระบบ</h1><br>
								<?php
								$form=array('accept-charset'=>'utf-8','class'=>'form-horizontal','id'=>'frm-login','name'=>'frm-login'); 
								echo form_open('/user/login/check', $form);
								echo form_hidden('base_url',base_url());
								echo form_hidden('site_url',site_url());

								?>
								<input type="text" name="username" id="username_login" placeholder="Username">
								<input type="password" name="password" id="password_login" placeholder="Password">
								<div id="form-success-div_login" class="text-success"></div> 
								<div id="form-error-div_login" class="text-danger"></div>
								<button type="submit" name="login" class="login btn-login" >เข้าสู่ระบบ</button> 
								<a href="<?=$authUrl?>" class="login btn-facebook"><i class="fa fa-facebook"></i> เข้าสู่ระบบด้วย Facebook</a>

								<a href="<?php echo site_url("user/register/") ?>" class="login btn-register-l">สมัครสมาชิก</a>
								<?php echo form_close(); ?>

								<div class="login-help" style="text-align: right;">
									<a href="#" onclick="forgotShow()">ลืมรหัสผ่าน ?</a> 
								</div>
							</div>
							<div class="loginmodal-container" id="forgotForm" style="display: none;">
								<h1>ลืมรหัสผ่าน</h1><br>
								<?php
								$form=array('accept-charset'=>'utf-8','class'=>'form-horizontal','id'=>'frm-forgot','name'=>'frm-forgot'); 
								echo form_open('/user/forgot/check', $form);
								echo form_hidden('base_url',base_url());
								echo form_hidden('site_url',site_url());

								?>
								<input type="text" name="username" id="username_forgot" placeholder="Username">
								<div class="alert_username_forgot"></div>
								<input type="text" name="email" id="email_forgot" placeholder="Email">
								<div class="alert_email_forgot"></div>
								
								<button type="submit" name="forgot" class="login btn-login" ><span id="form-img-div_forgot"></span> ยืนยัน</button> 
								<div id="form-success-div_forgot" class="text-success"></div> 
								<div id="form-error-div_forgot" class="text-danger"></div>
								<!-- <a href="<?=$authUrl?>" class="login btn-facebook"><i class="fa fa-facebook"></i> เข้าสู่ระบบด้วย Facebook</a>

								<a href="<?php echo site_url("user/register/") ?>" class="login btn-register-l">สมัครสมาชิก</a>
								<?php echo form_close(); ?> -->

								<div class="login-help" style="text-align: right;">
									<a href="#" onclick="loginShow()">เข้าสู่ระบบ</a> 
								</div>
							</div>


						</div>

					</div>
					<div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        	<button type="button" class="btn btn-primary">Save changes</button> -->
                     </div>
                </div>
            </div>
        </div>

<!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

Your customer chat code
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="122499241186246">
</div> -->

<!-- cahat -->

<div class="floating-chat enter">
	<a href="https://m.me/gorradesign" target="_blank" class="chat_a">
		<i class="fa fa-comments chat" aria-hidden="true"></i>
		<div class="chat">
			<div class="header">

			</div>
		</div>
	</a>
</div>
