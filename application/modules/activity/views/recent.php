<section class="blog-area">
			<div class="container">
				<div class="separator-4"></div>
				<div class="blog-activity">
					<div class="single-post">
						<div class="title-blog">
							<h3>
								<b class="light-color">เรียนสดกับ อ.กอร่า</b>
							</h3>
						</div>
						<div class="row">
							<div class="col-lg-7 col-md-12 col-ms-12 ">
								<div class="">
									<div class="image-wrapper">

										<img src="<?php echo $info_recent['image'];?>" alt="Blog Image">

										
									</div>	
								</div><!-- single-post -->
								
							</div>
							<div class="col-lg-5 col-md-12 col-ms-12 ">
								<div class="content-activity">
									<span class="title"><?php echo $info_recent['title'];?></span>
									<p><?php echo $info_recent['excerpt'];?></p>
									<ul>
										<li>วัน : <?php echo $info_recent['date'];?></li>
										<li>เวลา : <?php echo $info_recent['time'];?></li>
										<li>สถานที่ : <?php echo $info_recent['location'];?></li>
									</ul>
									<?php if($info_recent['promotion']!=""){ ?>
									<ul>
										<li>ราคา : <span style="font-size:18px; text-decoration: line-through;text-decoration-color: red;"><?php echo number_format($info_recent['price']);?> </span> บาท</li>
										<li>โปรโมชั่น : <?php echo number_format($info_recent['promotion']);?> บาท</li>
									</ul>
								    <?php }else{ ?>
									<ul>
										<li>ราคา : <?php echo number_format($info_recent['price']);?> บาท</li>
										
									</ul>
								   <?php } ?>

									<ul>
									<center><a  class="button-click" href="<?php echo site_url("activity/detail/{$info_recent['linkId']}");?>"><span style="color: #f7f007">
												คลิก!! ดูรายละเอียด </span><img src="<?php echo base_url('assets/website/images/icon/click.png')?>" style="width: 30px">
											    </a>
									</center>
								    </ul>
								</div><!-- single-post -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- section -->