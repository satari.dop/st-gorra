<!-- breadcumb-area start -->
        <div class="breadcumb-area bg-img-5 black-opacity">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcumb-wrap text-center">
                            <h2>บทความ</h2>
                            <ul>
                                <li><a href="<?php echo site_url('home');?>">หน้าหลัก</a></li>
                                <li>/</li>
                                <li ><a href="<?php echo site_url('article');?>">บทความ</a></li>
                                 <li>/</li>
                                 <li class="active"><?php echo $name;?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcumb-area end -->

        <!-- blog-area start -->
        <div class="blog-area bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <div class="row">
                            <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
                            <div class="col-xs-12">
                                <div class="blog-wrap">
                                    <div class="blog-img">
                                        <img src="<?php echo $rs['image']; ?>" alt="">
                                    </div>
                                    <div class="blog-content">
                                        <ul class="blog-meta">
                                            <li><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_language($rs['createDate'],false,'th') ?></a></li>
                                            
                                        </ul>
                                        <h3><?php echo $rs['title'] ?></h3>
                                        <p><?php echo $rs['excerpt'] ?></p>
                                        <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">อ่านต่อ <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <?php } } ?>

                            
                            <div class="col-xs-12">
                                 <?php //echo $links ;?>
                                 <div class="pagination-wrap text-center">
                                  <?php echo $links ;?>
                                   <!-- <ul>
                                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li> <span>3</span></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                    </ul>-->
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <aside class="sidebar-wrap">
                            <div class="search-wrap widget">
                                <form action="<?php echo site_url('article');?>" id="search_form"> 
                                    <input type="text" placeholder="ค้นหา..." name="keyword" value="<?=$keyword?>">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <div class="widget sidebar-menu">
                                <h3 class="widget-title">หมวดหมู่</h3>
                                <ul>
                                    <?php foreach ($category as $key => $value) { 
                                    $link = str_replace(" ","-",$value['name']);?>        
                                    <li><a href="<?php echo site_url("article/category/{$link}");?>"><?=$value['name']?></a></li>
                                  <?php } ?>
                                </ul>
                            </div>
                           
                            <div class="widget recent-post">
                                <h3 class="widget-title">บทความแนะนำ</h3>
                                <ul>
                                    <li>
                                        <div class="post-img">
                                            <img src="assets/images/post/1.jpg" alt="">
                                        </div>
                                        <div class="post-content">
                                            <a href="#">Vivamus ut mauris a dui fringilla mattis a sit amet lectus.</a>
                                            <p>20 JUN 2017</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-img">
                                            <img src="assets/images/post/2.jpg" alt="">
                                        </div>
                                        <div class="post-content">
                                            <a href="#">Nam lacus dolor, egestas eget ultrices sed, congue eget lorem.</a>
                                            <p>15 JUN 2017</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-img">
                                            <img src="assets/images/post/3.jpg" alt="">
                                        </div>
                                        <div class="post-content">
                                            <a href="#">Mauris vel ullamcorper mauris, eu faucibus est.</a>
                                            <p>12 JUN 2017</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog-area end -->