<section class="section blog-article">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="blog-posts">
					<div class="title">
						<h3>บทความกราฟฟิกล่าสุด</h3>
						<!-- <p>ดูทั้งหมด</p> -->
						<!-- <div class="separator"></div> -->
					</div>
					<?php foreach ($info_recent as $key => $rs) { ?>
						
						<div class="row">
							<div class="col-lg-4 col-md-6 col-ms-12 ">
								<div class="hover13 column">
									<a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">
										<figure>
											<img src="<?php echo $rs['image']; ?>" alt="Blog Image">
										</figure>
									</a>
								</div>
							</div>
							<div class="col-lg-5 col-md-6 col-ms-12 ">
								<div class="content-activity">
									<h4 ><a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a></a></h4>
									<p>หมวดหมู่ : <?php echo $rs['name'] ?></p>
									<span class="post-time">โพสเมื่อ : <?php echo date_language($rs['createDate'],true,'th') ?> อ่าน : <?php echo number_format($rs['view']); ?> </span>
									<p><?php echo $rs['excerpt'] ?></p>

									<div class="tags-area">
										<ul class="tags">
											<?php if(!empty($rs['tags'])){  foreach ($rs['tags'] as $tg => $tg_) { 
												$linkTags = str_replace(" ","-",$tg_);
												
												?>
												
												<li><a href="<?php echo site_url("article/tags/{$linkTags}");?>" class="btn" href="#"><?php echo $tg_; ?></a></li>
												
											<?php } } ?>

										</ul>
									</div>
								</div>
							</div>
						</div><!-- row -->
						
					<?php } ?>



				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->
		</div>	
	</div>
		</section><!-- section -->