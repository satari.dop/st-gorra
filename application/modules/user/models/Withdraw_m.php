<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Withdraw_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    
    
    public function insertWithdraw($value) {
        $this->db->insert('point_withdraw', $value);
        return $this->db->insert_id();
    }
    public function get_withdraw($id) 
    {
        
        $this->db->where('a.point_withdrawId', $id);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.firstname,b.lastname,b.email')
                        ->from('point_withdraw a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        ->get();
        return $query;
    }
    public function get_withdraw_bank($param) 
    {
        
        $this->db->where('a.bankCode', $param['bankCode']);
        $this->db->where('a.userId',$param['userId']);
        $this->db->order_by('a.createDate','desc');
        $query = $this->db
                        ->select('a.*')
                        ->from('point_withdraw a')
                        ->get();
        return $query;
    }

    public function insert_point_log($value) {

        $this->db->insert('point_log', $value);

        $id = $this->db->insert_id();

        return $id;

    }
    
    
    
}
