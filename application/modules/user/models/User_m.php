<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) {
        
            
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }  
        if ( isset($param['member_typeId']) && $param['member_typeId'] != "" ) {
            $this->db->where('a.member_typeId', $param['member_typeId']);
        }         
       
        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);

        if ( isset($param['username']) ) 
            $this->db->where('a.username', $param['username']);
        
        if ( isset($param['userId']) ) 
            $this->db->where('a.userId', $param['userId']);

        if ( isset($param['password']) ) 
            $this->db->where('a.password', $param['password']);

        if ( isset($param['oauth_uid']) ) 
            $this->db->where('a.oauth_uid', $param['oauth_uid']);

        // if ( !in_array($this->router->method, array("profile","check_password","check_email")))
        //     $this->db->where('a.userId !=', $this->session->user['userId']);
        
        if ( isset($param['recycle']) ) {
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
        }
        if ( $this->session->user['type'] != 'developer')
            $this->db->where('a.type !=', 'developer');

        
    }
    
    public function insert($value) {
        $this->db->insert('user', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('userId', $id)
                        ->update('user', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('userId', $id)
                        ->update('user', $value);
        return $query;
    }
    
    public function update_last_login()
    {
        $this->db->where('userId', $this->session->user['userId'])
                ->update('user', array('lastLogin'=>db_datetime_now()));
    }  
     
    
}
