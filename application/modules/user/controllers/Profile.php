<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->library('encryption');
        $this->load->model('user_m');
        $this->load->model('withdraw_m');
        $this->load->model('front/front_m');
        
    }


    public function index()
    {
        if ($this->session->member['userId'] == 0) 
            redirect(site_url("home"));


        $this->load->module('front');

        $input['userId']=$this->session->member['userId'];
        $info = $this->user_m->get_rows($input);

        $data['profile']=$info->row();
        //arr($data['profile']);exit();
        $data['frmAction'] = site_url("user/profile/save");
        $data['contentView'] = 'user/profile/form';
        $data['pageScript'] = 'assets/scripts/user/profile/index.js';
        
        $this->front->layout($data);
    }
    public function save()
    {

        $input = $this->input->post();

	    $ip = $_SERVER['REMOTE_ADDR'];

	    
    	if($this->input->post('robot') != null){
    		$captcha = "ok";
    		$responseKeys["success"] = 1;
    	} else {
    		$captcha = false;
    		$responseKeys["success"] = false;
    	}


	    if(intval($responseKeys["success"]) !== 1||!$captcha) {
		    echo json_encode(array('info_txt'=>'error','msg' => '* Captcha error','msg2'=>'กรุณาลองใหม่อีกครั้ง!'));
	    } else {
            $userId=$input['userId'];
            
            if($input['passwordOld']!=""){
              
               
                $password=$this->check_password($userId,$input['passwordOld']);
                
                if($password == 0){

                    $resp_msg = array('info_txt'=>"error_passwordOld",'msg'=>'รหัสผ่านเดิมไม่ถูกต้อง');
                    echo json_encode($resp_msg);
                    return false;
                    
                }
            }
	       

	        $value = $this->_build_data($input);
	        $userId = $this->user_m->update($userId,$value);
	        if ( $userId ) {

	             $resp_msg = array('info_txt'=>"success",'msg'=>'แก้ไขข้อมูลสำเร็จ','msg2'=>'กรุณารอสักครู่...');
	                echo json_encode($resp_msg);
	                return false;
	        } else {
	            $resp_msg = array('info_txt'=>"error",'msg'=>'แก้ไขข้อมูลไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	            echo json_encode($resp_msg);
	            return false;
	        }
	    }
    }

    public function success()
    {

        $this->load->module('front');
        
        $data['contentView'] = 'user/register/success';
        
        
        $this->front->layout($data);
    }

    private function _build_data($input) {
       
        
        $value['firstname'] = $input['fname'];
        $value['lastname'] = $input['lname'];
        $value['phone'] = $input['tel'];
        $value['email'] = $input['email'];
        

        if($input['password']!=""){
             $value['password'] = $this->encryption->encrypt($input['password']);
        }

        
        $value['updateDate'] = db_datetime_now();
        $value['updateBy'] = $this->session->user['userId'];
        
        return $value;
    }

    // public function check_password($userId,$passwordOld)
    // {
        
    //     $input['userId'] = $userId;
    //     $input['password']=$this->encryption->encrypt($passwordOld);
    //     //$input['password'] = $passwordOld;
    //     $info = $this->user_m->get_rows($input);
    //    // arrx($input);
    //     if ( $info->num_rows() > 0 ) {         
    //         $rs = 1;
    //     } else {
    //         $rs =  0;
    //     }
    //     return $rs;
    // }

    public function check_password($userId,$passwordOld) {
        
        $input['userId'] = $userId;
        $input['recycle'] = 0;
        $info = $this->user_m->get_rows($input);
        $row = $info->row();
        //print_r($row);exit();
        if ( $this->encryption->decrypt( $row->password ) == $passwordOld  ) {
            $rs = 1;
        } else {
            $rs = 0;
        }

        return $rs;
        
    }

    public function deposit() {
        
          if ($this->session->member['userId'] == 0) 
            redirect(site_url("home"));


        $this->load->module('front');

        $userId=$this->session->member['userId'];
       
        $info= $this->db
                ->select('a.*')
                ->select('b.*')
                ->from('point_log a')
                ->join('course_register b', 'a.course_registerId = b.course_registerId', 'left')
                ->where('b.status',1)
                ->where('a.userId',$userId)
                ->group_by('code')
                ->get()->result();

        $info2= $this->db
                ->select('a.*')
                ->select('b.*')
                ->from('point_log a')
                ->join('point_withdraw b', 'a.point_withdrawId = b.point_withdrawId', 'left')
                ->where('a.userId',$userId)
                ->where('b.point_withdrawId !=',"")
                ->group_by('code')
                ->get()->result();
        $sum=0;
        $info_r=array();
        foreach ($info as $key => $rs) {
            $info_r['1'.$key]['code']=$rs->code;
            $info_r['1'.$key]['date']=date_language($rs->created_at,true,'th');
            $info_r['1'.$key]['action']="เงินเข้า";
            $info_r['1'.$key]['amount']=number_format($rs->point);
            $info_r['1'.$key]['status']="สำเร็จ";
            $sum=$sum+$rs->point;
        }
        foreach ($info2 as $key => $rs) {
            if($rs->status=='0'){
                $act="อยู่ระหว่างการตรวจสอบ";
            }else{
                $act="สำเร็จ";
            }
            $info_r['2'.$key]['code']=$rs->code;
            $info_r['2'.$key]['date']=date_language($rs->created_at,true,'th');
            $info_r['2'.$key]['action']="ถอนเงิน";
            $info_r['2'.$key]['amount']=number_format($rs->point);
            $info_r['2'.$key]['status']=$act;
            $sum=$sum+$rs->point;
        }
        //arr($info_r);exit();
        // $info.= $this->db
        //         ->select('a.*')
        //         ->select('b.*')
        //         ->from('point_log a')
        //         ->join('course_register b', 'a.course_registerId = b.course_registerId', 'left')
        //         ->where('b.status',1)
        //         ->where('a.userId',$userId)
        //         ->get()->result();

        // $amount = $this->db
        //         ->select('sum(a.point) as amount ')
        //         ->from('point_log a')
        //         ->join('course_register b', 'a.course_registerId = b.course_registerId', 'left outer')
        //         ->join('point_withdraw c', 'a.point_withdrawId = c.point_withdrawId', 'left outer')
        //         ->where('a.userId',$userId)
        //         ->where('b.status',1)
        //         // ->group_start()
        //         //     ->where('a.userId',$userId)
        //         //     ->or_where('b.status',1)
        //         // ->group_end()
                
        //         ->get()->row();

        // //arr($amount->amount);exit();
       
        $data['info']=$info_r;
        $data['amount']=$sum;
       
        $data['contentView'] = 'user/profile/deposit';
        $data['pageScript'] = 'assets/scripts/user/profile/deposit.js';
        
        $this->front->layout($data);
        
    }

    public function withdraw() {

         $this->load->module('front');
         $data['bank_list'] = $this->db
                ->select('a.*')
                ->from('bank_list a')
                ->get()->result();

         $userId=$this->session->member['userId'];
       
        $info= $this->db
                ->select('a.*')
                ->select('b.*')
                ->from('point_log a')
                ->join('course_register b', 'a.course_registerId = b.course_registerId', 'left')
                ->where('b.status',1)
                ->where('a.userId',$userId)
                ->group_by('code')
                ->get()->result();

        $info2= $this->db
                ->select('a.*')
                ->select('b.*')
                ->from('point_log a')
                ->join('point_withdraw b', 'a.point_withdrawId = b.point_withdrawId', 'left')
                ->where('a.userId',$userId)
                ->where('b.point_withdrawId !=',"")
                ->group_by('code')
                ->get()->result();
        $sum=0;
        $info_r=array();
        foreach ($info as $key => $rs) {
            $info_r['1'.$key]['code']=$rs->code;
            $info_r['1'.$key]['date']=date_language($rs->created_at,true,'th');
            $info_r['1'.$key]['action']="เงินเข้า";
            $info_r['1'.$key]['amount']=number_format($rs->point);
            $info_r['1'.$key]['status']="สำเร็จ";
            $sum=$sum+$rs->point;
        }
        foreach ($info2 as $key => $rs) {
            if($rs->status=='0'){
                $act="รอการตรวจสอบ";
            }else{
                $act="สำเร็จ";
            }
            $info_r['2'.$key]['code']=$rs->code;
            $info_r['2'.$key]['date']=date_language($rs->created_at,true,'th');
            $info_r['2'.$key]['action']="ถอนเงิน";
            $info_r['2'.$key]['amount']=number_format($rs->point);
            $info_r['2'.$key]['status']=$act;
            $sum=$sum+$rs->point;
        }

        $data['info']=$info_r;
        $data['amount']=$sum;

         $data['frmAction'] = site_url("user/profile/withdraw_save");
         $data['contentView'] = "user/profile/withdraw";
         $data['pageScript'] = 'assets/scripts/user/profile/withdraw.js';
         $this->front->layout_iframe($data);
    }

    public function withdraw_check_bank() {

        $input = $this->input->post();
        $input['userId'] = $this->session->member['userId'];
        $withdraw_bank=$this->withdraw_m->get_withdraw_bank($input)->row();

        if(!empty($withdraw_bank)){
            $resp_msg = array('info_txt'=>"success",'msg'=>'พบบัญชี','result'=>$withdraw_bank);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ไม่พบบัญชี');
                echo json_encode($resp_msg);
               return false;
        }

    }

    public function withdraw_save() {

        $input = $this->input->post();
        $value=$this->_build_data_withdraw($input);
        $result = $this->withdraw_m->insertWithdraw($value);

        if($result){
            $data['withdraw_data']=$this->withdraw_m->get_withdraw($result)->row();
            $this->update_point_log($result);

            
            $email=$data['withdraw_data']->email;

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('user/profile/mail/mailer_form_w.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : แจ้งถอนเงิน';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('user/profile/mail/mailer_form_w_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : แจ้งถอนเงิน';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

            
            
            $resp_msg = array('info_txt'=>"success",'msg'=>'แจ้งถอนเงินเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'แจ้งถอนเงินไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
    }

    public function withdraw_success($id) {
        $this->load->module('front');
         $data['withdraw']=$this->withdraw_m->get_withdraw($id)->row();
         $data['contentView'] = "user/profile/withdraw_success";
         $this->front->layout_iframe($data);
    }

    public function update_point_log($id){

        $withdraw_data=$this->withdraw_m->get_withdraw($id)->row();

        $data['action']='withdraw';
        $data['point']= "-".$withdraw_data->price;
        $data['userId']=$withdraw_data->userId;
        $data['point_withdrawId']=$id;
        $data['courseId']="";
        $data['note']='ถอนเงิน';
        $data['created_at']=db_datetime_now();
        $data['created_by']=$this->session->member['userId'];

        $this->withdraw_m->insert_point_log($data);
            
    }

    private function _build_data_withdraw($input) {
        $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.'withdraw'.'/';
        create_dir($source_path); 
        
        $value['code'] = "W".time();
        $value['userId'] = $this->session->member['userId'];
        $value['bankCode'] = $input['bankCode'];
        $value['bankName'] = $input['bankName'];
        $value['bankNo'] = $input['bankNo'];
        $value['price'] = str_replace(",","",$input['price']);
        $value['status'] = 0;

        $value['createDate'] = db_datetime_now();
        $value['updateDate'] = db_datetime_now();

 
        //$value['filePath']=$source_path.'/';

        if(isset($_FILES['fileBookbank'])&&$_FILES['fileBookbank']['tmp_name'])
       {
              
              $date_file = date('Y').'_'.date('YmdHis');
              $config=array(
                'upload_path' => $source_path,
                'allowed_types' => 'gif|jpg|jpeg|png|pdf',
                'file_name' => $date_file.'1',
                'max_size' => '20480',
              );    

            $this->upload->initialize($config);
            if( ! $this->upload->do_upload('fileBookbank'))
            { 
              $resp_msg = array('info_txt'=>"UploadLinkThumbnail",'text'=> $this->upload->display_errors());
              echo json_encode($resp_msg);
              return false;
            }
            else
            {
                $data_thumb1 = array('fileBookbank' => $this->upload->data('fileBookbank'));              
                $extendsion = explode(".", $_FILES["fileBookbank"]["name"]);
                $extendsion = $extendsion[(count($extendsion) - 1)];
                $attach= $date_file.'1.'.$extendsion;


                    $config_re=array(
                      'image_library' => 'GD2',
                      'quality'=> '100%',
                      'source_image' => $source_path.$attach,
                      'create_thumb' => FALSE,
                      'maintain_ratio' => FALSE
                    );

                    $this->image_lib->initialize($config_re);

                    $this->image_lib->resize();   
                    $this->image_lib->clear();
                    
                    if(!empty($attach)){
                       //@unlink(FCPATH .$input['fileBookbank_old']);
                       $value['fileBookbank']=$source_path.$attach;
                     
                    }
              }

        }else{
            $value['fileBookbank']=$input['fileBookbank_old'];
        }

        if(isset($_FILES['fileEvidence'])&&$_FILES['fileEvidence']['tmp_name'])
       {
               
              $date_file = date('Y').'_'.date('YmdHis');
              $config=array(
                'upload_path' => $source_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'file_name' => $date_file.'2',
                'max_size' => '20480',
              );    

            $this->upload->initialize($config);
            if( ! $this->upload->do_upload('fileEvidence'))
            { 
              $resp_msg = array('info_txt'=>"UploadLinkThumbnail",'text'=> $this->upload->display_errors());
              echo json_encode($resp_msg);
              return false;
            }
            else
            {
                $data_thumb1 = array('fileEvidence' => $this->upload->data('fileEvidence'));              
                $extendsion = explode(".", $_FILES["fileEvidence"]["name"]);
                $extendsion = $extendsion[(count($extendsion) - 1)];
                $attach2= $date_file.'2.'.$extendsion;


                    $config_re=array(
                      'image_library' => 'GD2',
                      'quality'=> '100%',
                      'source_image' => $source_path.$attach2,
                      'create_thumb' => FALSE,
                      'maintain_ratio' => FALSE
                    );

                    $this->image_lib->initialize($config_re);

                    $this->image_lib->resize();   
                    $this->image_lib->clear();
                    
                    if(!empty($attach)){
                      // @unlink(FCPATH .$input['fileEvidence_old']);
                      $value['fileEvidence']=$source_path.$attach2;
                      
                    }
              }

        }else{
            $value['fileEvidence']=$input['fileEvidence_old'];
        }
     
    //arr($value);exit();
        return $value;
    }

    
}