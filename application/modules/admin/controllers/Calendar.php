<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Calendar extends MX_Controller {

    private $_title = "ปฏิทินสินค้าเกษตร";
    private $_pageExcerpt = "การจัดการฐานข้อมูลเกี่ยวกับปฏิทินสินค้าเกษตร";
    private $_grpContent = "calendar";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
      
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("calendar_m");
        
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[2][] = action_import(site_url("admin/{$this->router->class}/import"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $info = $this->calendar_m->get_rows($input);
        $infoCount = $this->calendar_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->calendarId);
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['framingYear'] = $rs->framingYear;
            $column[$key]['product'] = $rs->product;
            $column[$key]['year'] = $rs->year;
            $column[$key]['month'] = thai_month_abbr($rs->month);
            $column[$key]['unit'] = $rs->unit;
            $column[$key]['quantity'] = number_format($rs->quantity,2);
            $column[$key]['percent'] = $rs->percent;
            $column[$key]['status'] = $rs->status;
            $column[$key]['adjustDate'] = $rs->adjustDate;
            $column[$key]['forcastMonth'] = $rs->forcastMonth;
            $column[$key]['forcastYear'] = $rs->forcastYear;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
      
    public function import(){
        $inputFileType = 'Xls';
        $inputFileName = 'uploads/import/calendar.xls';
        $backupFileName = "uploads/import/backup/calendar".time().".xls";
        $result = false;
        if ( is_file($inputFileName) ) {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($inputFileName);
            $spreadsheet->setActiveSheetIndex(0);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            foreach ( $sheetData as $key => $rs) {
                if ( $key >= 2 && $rs['A']) {
                    $value[] = [
                        'framingYear' => trim($rs['A']),
                        'product' => trim($rs['B']),
                        'year' => intval($rs['C']),
                        'month' => intval($rs['D']),
                        'unit' => trim($rs['E']),
                        'quantity' => intval(preg_replace('/[^\d.]/', '', $rs['F'])),
                        'percent' => intval(preg_replace('/[^\d.]/', '', $rs['G'])),
                        'status' => intval(preg_replace('/[^\d.]/', '', $rs['H'])),
                        'adjustDate' => trim($rs['I']),
                        'forcastMonth' => trim($rs['J']),
                        'forcastYear' => trim($rs['K']),
                    ];
                }
            }
            $this->db->empty_table('calendar');
            $result = $this->db->insert_batch('calendar', $value);
            if ( $result ) {
                $error = 0;
//                rename($inputFileName, $backupFileName);
            } else {
                $error = 1;
                $errorMsg = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
        } else {
            $error = 2;
            $errorMsg = 'ไม่พบไฟล์ที่นำเข้า';
        }

        if ( $error == 0 ) {
            $data['success'] = true;
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'การนำเข้าข้อมูลเรียบร้อย';
        } else {
            $data['success'] = false;
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = $errorMsg;
        }
        $data['toastr'] = $toastr;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));  
        
    }
     
    
}
