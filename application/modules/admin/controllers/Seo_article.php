<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Seo_article extends MX_Controller {

    private $_title = "ข้อมูล SEO บทความ";
    private $_pageExcerpt = "";
    private $_grpContent = "seo_article";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("seo_m");
        $this->load->model("upload_m");
        $this->load->library('image_moo');
    }
    
    public function edit() {
        $this->load->module('admin/admin');
        
        $input['grpContent'] = $this->_grpContent;
        $info = $this->seo_m->get_rows($input);
       
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;

        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->seoId, $this->_grpContent, 'coverImage');

        //arr($data['coverImage']);exit();
       
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");

       // arr($info);exit();
        if (!empty($info)) {
           $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        }
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/seo/form";
        
        $this->admin->layout($data);
    }    

    
    public function save() {
        $input = $this->input->post(null, true);
        $images=$this->upload_ci('input-file-preview');
        $input['coverImageId']=$images['insertId'];

        $value = $this->_build_data($input);
        $result = $this->seo_m->insert($value);
        if ( $result ) {
            $value = $this->_build_upload_content($result, $input);
            Modules::run('admin/upload/update_content', $value);
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/edit"));
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $images=$this->upload_ci('input-file-preview');
            $input['coverImageId']=$images['insertId'];
        }

        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->seo_m->update($id, $value);
        if ( $result ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/edit"));
    }
    
    private function _build_data($input) {
        
        $value['metaTitle'] = $input['metaTitle'];
        $value['metaDescription'] = $input['metaDescription'];
        $value['metaKeyword'] = $input['metaKeyword'];
        $value['grpContent'] = $this->_grpContent;
        $value['createDate'] = db_datetime_now();
        $value['createBy'] = $this->session->user['userId'];
        $value['updateDate'] = db_datetime_now();
        $value['updateBy'] = $this->session->user['userId'];
       
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) ) {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId']
            );
        } else {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => null,
            );
        }
        return $value;
    }  
    
    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
             $this->resizeImage($result['file_name']);
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function resizeImage($filename){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => 600,
          'height' => 388
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   }  
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 730;
        $config['height'] = 487;
    
        
        return $config;
    } 
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->repo_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->repo_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->repo_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->repo_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}
