<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Position extends MX_Controller {

    private $_title;
    private $_pageExcerpt;
    private $_grpContent = "position";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->_treeData = new stdClass();
        $this->load->model("department_m");
        $this->load->library('ckeditor');
    }
    
    public function index()
    {
        $this->load->module('admin/admin');
        
        $data['departmentType'] = 'position';
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        //$action[1][] = action_filter();
        $action[1][] = action_add(base_url("admin/{$this->router->class}/create"));
        //$action[2][] = action_order(base_url("admin/{$this->router->class}/order"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));

        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตำแหน่ง", "javascript:void(0)");
       
        


        // page detail
        $this->_pageExcerpt = $this->_pageExcerpt."ตำแหน่ง";
        
        
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/index.js";

        $this->admin->layout($data);
    }
    
    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->department_m->get_rows($input);
        $infoCount = $this->department_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rs) {
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rs->name, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->info->departmentId);
                $action = array();
                $action[] = table_edit(site_url("admin/{$this->router->class}/edit/{$input['departmentType']}/{$id}"));
                $action[] = table_trash("admin/{$this->router->class}/action/trash");
                $active = $rs->info->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['order'] = $rs->info->order;
                $column[$key]['name'] = $rs->name;
                $column[$key]['excerpt'] = $rs->info->excerpt;
                $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
                $column[$key]['createDate'] = datetime_table($rs->info->createDate);
                $column[$key]['updateDate'] = datetime_table($rs->info->updateDate);
                $column[$key]['action'] = implode($action," ");
                $key++;
            }
        }        
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function trash()
    {
        $this->load->module('admin/admin');
        
        $data['departmentType'] = 'position';
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb  
        $data['breadcrumb'][] = array("ตำแหน่ง",  base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('รายการถังขยะ', "javascript:void(0)");
        
        // page detail
        $this->_pageExcerpt = $this->_pageExcerpt."ตำแหน่ง";
        
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/trash.js";

        $this->admin->layout($data);
    }
    
    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->department_m->get_rows($input);
        $infoCount = $this->department_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rs) {
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rs->name, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->info->departmentId);

                $action = array();
              
                $action[] = table_restore("admin/{$this->router->class}/action/restore");
                
                $active = $rs->info->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['order'] = $rs->info->order;
                $column[$key]['name'] = $rs->name;
                $column[$key]['excerpt'] = $rs->info->excerpt;
                
                $column[$key]['recycleDate'] = datetime_table($rs->info->recycleDate);
                $column[$key]['action'] = implode($action," ");
                $key++;
            }
        }        
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');
        
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        $data['departmentType'] = 'position';
        //$data['departmentDropDown'] = $this->department_drop_drown(true,$data['departmentType']);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตำแหน่ง", base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', "javascript:void(0)");
        

        // page detail
        $this->_pageExcerpt = $this->_pageExcerpt."ตำแหน่ง";
        
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/form.js";
        
        $this->admin->layout($data);
    }
    public function edit($departmentType="",$id) {
        $this->load->module('admin/admin');
        $id = decode_id($id);
        $info = $this->department_m->get_by_id($id);
        $data['info'] = $info;
        //print_r($info); exit();
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        $data['departmentType'] = $departmentType;
        //$data['departmentDropDown'] = $this->department_drop_drown(true,$departmentType);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/create"));
        

        // page detail
        $this->_pageExcerpt = $this->_pageExcerpt."ตำแหน่ง";
        
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/form.js";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $insertId = $this->department_m->insert($value);
        if ( $insertId ) {
            $input['departmentId'] = $insertId;
            $value = $this->_build_data_lang($input);
            $this->department_m->insert_lang($value);
            $value = array();
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }

     public function update() {

        $input = $this->input->post();
        $value = $this->_build_data($input);
        //$input['id'] = decode_id($input['id']);
        //print_r($value);exit();
        $result = $this->department_m->update($input['id'],$value);
         if ( $result ) {
            $input['departmentId'] = $input['id'];
            $value = $this->_build_data_lang($input);
            $this->department_m->update_lang($input['departmentId'],$value);
            $value = array();
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['type'] = $input['departmentType'];
       // $value['parentId'] = $input['parentId'];
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_data_lang($input) {
        
        $value['departmentId'] = $input['departmentId'];
        $value['name'] = $input['name'];
        $value['excerpt'] = isset ($input['excerpt']) ? $input['excerpt'] : NULL;
        $value['content'] = isset ($input['content']) ? $input['content'] : NULL;
        $value['lang'] = config_item('language_abbr');
        return $value;
    }
    
    public function order($departmentType="") {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}/order"));
        $action[1][] = action_list_view(base_url("admin/{$this->router->class}"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        $input['recycle'] = 0;
        $input['order'][0]['column'] = 1;
        $input['order'][0]['dir'] = 'asc';
        $input['departmentType'] = $departmentType;
        $info = $this->department_m->get_rows($input);
//        arrx($info->result());
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->departmentId];
            $thisref['id'] = $rs->departmentId;
            $thisref['content'] = $rs->name;
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['children'][] = &$thisref;
            } else {
                $treeData[] = &$thisref;
            }
        }
//        arr($treeData);
        $data['treeData'] = $treeData;
        $data['departmentType'] = $departmentType;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update_order");
        
        // breadcrumb
        if ( $departmentType == "news")
            $data['breadcrumb'][] = array("ข่าว", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('จัดลำดับ', base_url("admin/{$this->router->class}/order"));
        
        // page detail
        if ( $departmentType == "news") {
            $this->_pageExcerpt = $this->_pageExcerpt."ข่าว";
        }
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/order";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/order.js";

        $this->admin->layout($data);
    }
    
    public function update_order(){
        $input = $this->input->post();
        $order = json_decode($input['order']);
        $value = $this->_build_data_order($order);
        $result = $this->db->update_batch('department', $value, 'departmentId');
        if ( $result ) {
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/type/{$input['departmentType']}"));
    }
    
    private function _build_data_order($info, $parentId=0){
        $order = 0;
        foreach ($info as $rs) {
            $order++;
            $this->_orderData[$rs->id]['departmentId'] = $rs->id; 
            $this->_orderData[$rs->id]['order'] = $order;
            $this->_orderData[$rs->id]['parentId'] = $parentId;
            if ( isset($rs->children) )
                $this->_build_data_order ($rs->children, $rs->id);
        }
        return $this->_orderData;
    }
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->department_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->department_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 1;
                $value['recycle'] = 0;
                $result = $this->department_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->department_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function department_drop_drown($active=true,$departmentType) {
        $input['recycle'] = 0;
        if ( $active ) 
            $input['active'] = 1;
        
        $input['departmentType'] = $departmentType;
        $info = $this->department_m->get_rows($input);
        $tree = $this->_build_tree($info);
        $dropDown = $this->_print_drop_down($tree);
        return $dropDown;
    }
    
    private function _build_tree($info, $parentId=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parentId == $parentId) {
                $children = $this->_build_tree($info, $d->departmentId);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->departmentId} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->departmentId} = new stdClass();
            $this->_treeData->{$t->departmentId}->name = $dash . $t->name;
            $this->_treeData->{$t->departmentId}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeData;
    }
    
    private function _print_drop_down($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeDropDown[$t->departmentId] = $dash . $t->name;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_drop_down($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeDropDown;
    }    

    public function dropdown() {
        
        $info = $this->department_m->get_dropdown('position');
        $temp[''] = 'เลือกรายการ';
        foreach ( $info->result() as $rs ) $temp[$rs->departmentId] = $rs->name;
        
        return $temp;

          //print_r($info->result());
    }  

}
