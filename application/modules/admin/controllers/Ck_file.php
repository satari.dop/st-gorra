<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ck_file extends MX_Controller {
    
    public $filePath;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('ck_file_model', 'images');
        $this->load->helper('utf8');
        $this->load->library('images_oc');
        $this->filePath = 'uploads/ck/';
    }

    public function index() {
//        if (!$this->input->is_ajax_request()) return;
        $input = $this->input->get();
        $data['title'] = ('File manager');
        $data['base'] = base_url();
        $data['entry_folder'] = ('Folder');
        $data['entry_move'] = ('Move');
        $data['entry_copy'] = ('Copy');
        $data['entry_rename'] = ('Name');

        $data['button_folder'] = ('สร้างโฟล์เดอร์');
        $data['button_delete'] = ('ลบไฟล์');
        $data['button_move'] = ('ย้ายไฟล์');
        $data['button_copy'] = ('คัดลอก');
        $data['button_rename'] = ('เปลี่ยนชื่อ');
        $data['button_upload'] = ('อัพโหลด');
        $data['button_refresh'] = ('โหลดหน้านี้');
        $data['button_submit'] = ('ตกลง');

        $data['error_select'] = ('Select file error');
        $data['error_directory'] = ('Created Folder Error');

        $data['token'] = '';
        $data['directory'] = base_url("{$this->filePath}data/");
        $data['no_image'] = base_url('uploads/ck/no_image.png');

        if (isset($input['field'])) {
            $data['field'] = $input['field'];
        } else {
            $data['field'] = '';
        }

        if (isset($input['type'])) {
            $data['type'] = $input['type'];
        } else {
            $data['type'] = 0;
        }

        if (isset($input['CKEditorFuncNum'])) {
            $data['fckeditor'] = $input['CKEditorFuncNum'];
        } else {
            $data['fckeditor'] = false;
        }

        $this->load->view('ck_file/ck_filemanager', $data);
    }

    public function get_file() {
        $image = "data/".$this->input->get('image');
        $img = $this->images->resize(html_entity_decode($image, ENT_QUOTES, 'UTF-8'), 100, 100);
        $data = array();
        if ($image <> "") {
            $data['img'] = $img;   
            $info = pathinfo($image);
            $data['title'] = $info['basename'];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function directory() {
        $json = array();
        $input = $this->input->get();
        if (isset($input['directory'])) {
            $directories = glob(rtrim($this->filePath  . 'data/' . $input['directory'], '/') . '/*', GLOB_ONLYDIR);
            if ($directories) {
                $i = 0;
                foreach ($directories as $directory) {
                    $json[$i]['data'] = basename($directory);
                    $json[$i]['attributes']['directory'] = utf8_substr($directory, strlen($this->filePath  . 'data/'));
                    $children = glob(rtrim($directory, '/') . '/*', GLOB_ONLYDIR);
                    if ($children) {
                        $json[$i]['children'] = ' ';
                    }
                    $i++;
                }
            }
        }

        $this->output->set_output(json_encode($json));
    }

    public function files() {
        
        $json = array();
        $input = $this->input->get();
        
        if (!empty($input['directory'])) {
            $directory = $this->filePath  . 'data/' . $input['directory'];
        } else {
            $directory = $this->filePath  . 'data/';
        }

        $allowed = array(
            '.jpg',
            '.jpeg',
            '.png',
            '.gif',
            '.rar',
            '.pdf','.zip','.docx','.ppt','.doc','.xls','.xlsx',
        );

        $files = glob(rtrim($directory, '/') . '/*');

        if ($files) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    $ext = strrchr($file, '.');
                } else {
                    $ext = '';
                }

                if (in_array(strtolower($ext), $allowed)) {
                    $size = filesize($file);

                    $i = 0;

                    $suffix = array(
                        'B',
                        'KB',
                        'MB',
                        'GB',
                        'TB',
                        'PB',
                        'EB',
                        'ZB',
                        'YB'
                    );

                    while (($size / 1024) > 1) {
                        $size = $size / 1024;
                        $i++;
                    }

                    $json[] = array(
                        'filename' => basename($file),
                        'file' => utf8_substr($file, utf8_strlen($this->filePath  . 'data/')),
                        'size' => round(utf8_substr($size, 0, utf8_strpos($size, '.') + 4), 2) . $suffix[$i]
                    );
                }
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    public function create() {
        if (!$this->input->is_ajax_request()) return;
        $json = array();
        $input = $this->input->post();
//        arrx($input);
        if (isset($input['directory'])) {
            if (isset($input['name']) || $input['name']) {
                $directory = rtrim($this->filePath . 'data/' . $input['directory'], '/');
                
                if (!is_dir($directory)) {
                    $json['error'] = ('พบข้อผิดพลาด');
                }

                if (file_exists($directory . '/' .$input['name'])) {
                    $json['error'] = ('พบข้อผิดพลาดชื่อโฟลเดอร์');
                }
            } else {
                $json['error'] = ('พบข้อผิดพลาดชื่อโฟลเดอร์');
            }
        } else {
            $json['error'] = ('พบข้อผิดพลาด');
        }

        if (!isset($json['error'])) {
            mkdir($directory . '/' .$input['name'], 0777);

            $json['success'] = ('สร้างโฟลเดอร์สำเร็จ');
        }

        $this->output->set_output(json_encode($json));
    }

    public function delete() {
        if (!$this->input->is_ajax_request()) return;
        $json = array();
        $input = $this->input->post();
        if (isset($input['path'])) {
            $path = rtrim($this->filePath . 'data/' . str_replace('../', '', html_entity_decode($input['path'], ENT_QUOTES, 'UTF-8')), '/');
            if (!file_exists($path)) {
                $json['error'] = ('พบข้อผิดพลาดไม่พบไฟล์');
            }
            if ($path == rtrim($this->filePath . 'data/', '/')) {
                $json['error'] = ('พบข้อผิดพลาดไม่พบไฟล์');
            }
        } else {
            $json['error'] = ('พบข้อผิดพลาดไม่พบไฟล์');
        }
        if (!isset($json['error'])) {
            if (is_file($path)) {
                unlink($path);
            } elseif (is_dir($path)) {
                $files = array();
                $path = array($path . '*');
                while (count($path) != 0) {
                    $next = array_shift($path);

                    foreach (glob($next) as $file) {
                        if (is_dir($file)) {
                            $path[] = $file . '/*';
                        }

                        $files[] = $file;
                    }
                }
                rsort($files);
                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    } elseif (is_dir($file)) {
                        rmdir($file);
                    }
                }
            }
            $json['success'] = ('ลบไฟล์สำเร็จ');
        }
        $this->output->set_output(json_encode($json));
    }

    public function move() {
        if (!$this->input->is_ajax_request()) return;
        $json = array();
        $input = $this->input->post();
        if (isset($input['from']) && isset($input['to'])) {
            $from = rtrim($this->filePath . 'data/' . str_replace('../', '', html_entity_decode($input['from'], ENT_QUOTES, 'UTF-8')), '/');

            if (!file_exists($from)) {
                $json['error'] = ('พบข้อผิดพลาดไม่พบไฟล์');
            }

            if ($from == $this->filePath . 'data') {
                $json['error'] = ('พบข้อผิดพลาด');
            }

            $to = rtrim($this->filePath . 'data/' . str_replace('../', '', html_entity_decode($input['to'], ENT_QUOTES, 'UTF-8')), '/');

            if (!file_exists($to)) {
                $json['error'] = ('พบข้อผิดพลาดชื่อซ้ำ');
            }

            if (file_exists($to . '/' . basename($from))) {
                $json['error'] = ('พบข้อผิดพลาดชื่อซ้ำ');
            }
        } else {
            $json['error'] = ('พบข้อผิดพลาดไม่พบไฟล์');
        }

        if (!isset($json['error'])) {
            rename($from, $to . '/' . basename($from));

            $json['success'] = ('ย้ายไฟล์สำเร็จ');
        }

        $this->output->set_output(json_encode($json));
    }

    public function copy() {
        if (!$this->input->is_ajax_request()) return;
        $json = array();
        $input = $this->input->post();
        if (isset($input['path']) && isset($input['name'])) {
            if ((utf8_strlen($input['name']) < 3) || (utf8_strlen($input['name']) > 255)) {
                $json['error'] = ('พบข้อผิดพลาดเกี่ยวกับชื่อไฟล์');
            }
            $old_name = rtrim($this->filePath . 'data/' . str_replace('../', '', html_entity_decode($input['path'], ENT_QUOTES, 'UTF-8')), '/');

            if (!file_exists($old_name) || $old_name == $this->filePath . 'data') {
                $json['error'] = ('พบข้อผิดพลาดไม่สามารถคัดลอกได้');
            }

            if (is_file($old_name)) {
                $ext = strrchr($old_name, '.');
            } else {
                $ext = '';
            }

            $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($input['name'], ENT_QUOTES, 'UTF-8') . $ext);

            if (file_exists($new_name)) {
                $json['error'] = ('พบข้อผิดพลาดชื่อซ้ำ');
            }
        } else {
            $json['error'] = ('พบข้อผิดพลาดกรุณาเลือกไฟล์');
        }

        if (!isset($json['error'])) {
            if (is_file($old_name)) {
                copy($old_name, $new_name);
            } else {
                $this->recursiveCopy($old_name, $new_name);
            }

            $json['success'] = ('คัดลอกไฟล์สำเร็จ');
        }

        $this->output->set_output(json_encode($json));
    }

    function recursiveCopy($source, $destination) {
        $directory = opendir($source);

        @mkdir($destination);

        while (false !== ($file = readdir($directory))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($source . '/' . $file)) {
                    $this->recursiveCopy($source . '/' . $file, $destination . '/' . $file);
                } else {
                    copy($source . '/' . $file, $destination . '/' . $file);
                }
            }
        }

        closedir($directory);
    }

    public function folders() {
        $this->output->set_output($this->recursiveFolders($this->filePath . 'data/',1));
    }

    protected function recursiveFolders($directory, $f) {
        $output = '';
        if ($f == 1) {
            $output .= '<option value="' . utf8_substr($directory, strlen($this->filePath . 'data/')) . '">' . utf8_substr($directory, strlen($this->filePath . 'data/')) . 'Libraries</option>';
        } else {
            $output .= '<option value="' . utf8_substr($directory, strlen($this->filePath . 'data/')) . '">' . utf8_substr($directory, strlen($this->filePath . 'data/')) . '</option>';  
        }
        $directories = glob(rtrim(str_replace('', '', $directory), '/') . '/*', GLOB_ONLYDIR);

        foreach ($directories as $directory) {
            $f++;
            $output .= $this->recursiveFolders($directory, $f);
        }

        return $output;
    }

    public function rename() {
        if (!$this->input->is_ajax_request()) return;
        $json = array();
        $input = $this->input->post();
        if (isset($input['path']) && isset($input['name'])) {
            if ((utf8_strlen($input['name']) < 3) || (utf8_strlen($input['name']) > 255)) {
                $json['error'] = ('พบข้อผิดพลาดการตั้งชื่อ');
            }

            $old_name = rtrim($this->filePath . 'data/' . str_replace('../', '', html_entity_decode($input['path'], ENT_QUOTES, 'UTF-8')), '/');

            if (!file_exists($old_name) || $old_name == $this->filePath . 'data') {
                $json['error'] = ('พบข้อผิดพลาดชื่อซ้ำ');
            }

            if (is_file($old_name)) {
                $ext = strrchr($old_name, '.');
            } else {
                $ext = '';
            }

            $new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($input['name'], ENT_QUOTES, 'UTF-8') . $ext);

            if (file_exists($new_name)) {
                $json['error'] = ('พบข้อผิดพลาดชื่อซ้ำ');
            }
        }

        if (!isset($json['error'])) {
            rename($old_name, $new_name);

            $json['success'] = ('เปลี่ยนชื่อไฟล์สำเร็จ');
        }

        $this->output->set_output(json_encode($json));
    }

    public function upload() {
        $json = array();
        $input = $this->input->post();
//        print_r($input);exit;
        if (isset($input['directory'])) {
            if (isset($_FILES['image']) && $_FILES['image']['tmp_name']) {
                $filename = basename(html_entity_decode($_FILES['image']['name'], ENT_QUOTES, 'UTF-8'));
                if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
                    $json['error'] = ('error_filename');
                }

                $directory = rtrim($this->filePath . 'data/' . $input['directory'], '/');

                if (!is_dir($directory)) {
                    $json['error'] = ('upload direcotry error');
                }

                if ($_FILES['image']['size'] > 3000000000) {
                    $json['error'] = ('error_file_size');
                }

                /* $allowed = array(
                  'image/jpeg',
                  'image/pjpeg',
                  'image/png',
                  'image/x-png',
                  'image/gif',
                  'application/x-shockwave-flash'
                  );

                  if (!in_array($input['image']['type'], $allowed)) {
                  $json['error'] = ('error_file_type');
                  }

                  $allowed = array(
                  '.jpg',
                  '.jpeg',
                  '.gif',
                  '.png',
                  '.flv'
                  );

                  if (!in_array(strtolower(strrchr($filename, '.')), $allowed)) {
                  $json['error'] = ('error_file_type');
                  }

                  // Check to see if any PHP files are trying to be uploaded
                  $content = file_get_contents($input['image']['tmp_name']);

                  if (preg_match('/\<\?php/i', $content)) {
                  $json['error'] = ('error_file_type');
                  } */

                if ($_FILES['image']['error'] != UPLOAD_ERR_OK) {
                    $json['error'] = 'error_upload_' . $input['image']['error'];
                }
            } else {
                $json['error'] = ('อัพโหลดไฟล์พบข้อผิดพลาด-100');
            }
        } else {
            $json['error'] = ('อัพโหลดไฟล์พบข้อผิดพลาด-101');
        }

        // if (!$this->user->hasPermission('modify', 'common/filemanager')) {
        // $json['error'] = ('error_permission');
        // }

        if (!isset($json['error'])) {
            if (@move_uploaded_file($_FILES['image']['tmp_name'], $directory . '/' . $filename)) {
                $json['success'] = ('อัพโหลดไฟล์สำเร็จ');
               
                  // $this->images->resize700(html_entity_decode('/data/' . $input['directory'] . '/' . $filename, ENT_QUOTES, 'UTF-8'), 700, 220);
                    $this->images->resize(html_entity_decode('/data/' . $input['directory'] . '/' . $filename, ENT_QUOTES, 'UTF-8'), 100, 100);
           
            } else {
                $json['error'] = ('อัพโหลดไฟล์พบข้อผิดพลาด-102');
            }
        }

        $this->output->set_output(json_encode($json));
    }

}

?>