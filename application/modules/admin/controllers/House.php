<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class House extends MX_Controller {

    private $_title = "จัดการข้อมูลบ้านมือสอง";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับบ้านมือสอง";
    private $_grpContent = "house";
   

    public function __construct() 
    {
       parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("house_m");
        $this->load->model("config_m");

        $this->_location = array(""=>'เลือก', 'ยะลา'=>'ยะลา', 'ปัตตานี'=>'ปัตตานี','นราธิวาส'=>'นราธิวาส'); //array('ยะลา','ปัตตานี','นราธิวาส');
        $this->_statusBuy = array('ยังไม่ขาย','ขายแล้ว');
    }

    public function index() {
        $this->load->module('admin/admin');

        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";

        $this->admin->layout($data);
    }

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        //$statusBuy='<span class="label label-warning">ยังไม่ขาย</span>';
        //print_r($input); exit();
        $info = $this->house_m->get_rows($input);
       // print_r($info);exit();
        $infoCount = $this->house_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->contentId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;

            switch ($rs->statusBuy) {
                case 0 : $style = 'btn-default'; break; //ยังไม่ขาย
                case 1 : $style = 'btn-danger'; break; //ขายแล้ว
                default: $style = 'btn-default'; break;
            }
            $statusMethod = site_url("admin/house/action/statusBuy");
            $statusPicker = form_dropdown('statusPicker', $this->_statusBuy, $rs->statusBuy, "class='statusPicker' data-method='{$statusMethod}' data-id='{$id}' data-style='btn-flat {$style}'");
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['statusBuy'] = $statusPicker;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        $data['location'] = $this->_location;

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function save() {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $id = $this->house_m->insert($value);
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }

    private function _build_data($input) {
        $value['recommend'] = isset($input['recommend']) ? $input['recommend'] : 0;
        $value['title'] = str_replace(","," ",$input['title']);
        $value['categoryId'] = $input['categoryId'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['location'] = $input['location'];
        $value['longitude'] = $input['longitude'];
        $value['latitude'] = $input['latitude'];
        $value['price'] = str_replace(",","",$input['price']);
        
        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['title']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['title']);
        }

        $value['grpContent'] = $this->_grpContent;
        if ($input['mode'] == 'create') {
            $value['createDate'] = db_datetime_now();
            $value['updateDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
         //  print"<pre>";print_r($id);exit();
        $value = array();
        $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent
            );
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle']
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle']
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key1]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key1]
                );
            }
        }
        return $value;
    }

    public function edit($id = 0) {
        $this->load->module('admin/admin');

        $id = decode_id($id);
        $input['contentId'] = $id;
        $input['recycle'] = 0;
        $input['categoryId'] = '';
        $input['grpContent'] = $this->_grpContent;

        $data['location'] = $this->_location;

        $info = $this->house_m->get_rows($input);
        if ($info->num_rows() == 0) {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'ขอภัยไม่พบข้อมูลที่ต้องการแก้ไข');
            redirect($this->agent->referrer());
        }
        $info = $info->row();

        

        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'coverImage');
        $data['contentImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'contentImage');
        $data['galleryImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'galleryImage');
        $data['docAttach'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'docAttach');
        
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function update() {
        $input = $this->input->post();
       // print"<pre>";print_r($input);exit();
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->house_m->update($id, $value);
        if ( $result ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function trash() {
        $this->load->module('admin/admin');   
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['categoryId'] = '';
        $input['grpContent'] = $this->_grpContent;
        $info = $this->house_m->get_rows($input);
        $infoCount = $this->house_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->contentId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->house_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->house_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->house_m->update_in($input['id'], $value);
            }
            if ( $type == "statusBuy" ) {
                 $value['statusBuy'] = $input['statusBuy'];
                 $result = $this->house_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->house_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  


     
    
}
