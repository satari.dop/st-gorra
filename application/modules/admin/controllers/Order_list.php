<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Order_list extends MX_Controller {

    private $_title = "รายการสั่งซื้อ";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับรายการสั่งซื้อคอร์สเรียน";
    private $_grpContent = "order_list";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("order_list_m");
        $this->load->model("course/course_m");
        $this->load->model("course_m",'ca');
        $this->_status = array('รอการอนุมัติ','อนุมัติเรียบร้อย','ยกเลิก');
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf' => site_url("admin/{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        // $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
        // $action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        // $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        

        $input_u['active'] = 1;
        $input_u['recycle'] = 0;
        $courseExclude = $this->ca->get_rows($input_u)->result();
        $data['course']=array();
        $data['course']['']="ทั้งหมด";
        foreach ($courseExclude as $key => $value) {
            $data['course'][$value->courseId]=$value->title;
        }
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $info = $this->order_list_m->get_rows($input);
        $infoCount = $this->order_list_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_registerId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            //$column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";

             switch ($rs->status) {
                case 0 : $style = 'btn-warning'; break; //ยังไม่ขาย
                case 1 : $style = 'btn-success'; break; 
                case 2 : $style = 'btn-danger'; break; //ขายแล้ว
                default: $style = 'btn-default'; break;
            }
            $statusMethod = site_url("admin/order_list/action/status");
            $statusPicker = form_dropdown('statusPicker', $this->_status, $rs->status, "class='statusPicker' data-method='{$statusMethod}' data-id='{$id}' data-style='btn-flat {$style}'");

            $st="";
            if($rs->status==0){
                $st="<br><font color=red>(รอการอนุมัติ)</font>";
            }

             if(!empty($rs->couponCode)){ 
                if($rs->type==2){
                    $rt=($rs->price*$rs->discount)/100;
                    $total=$rs->price-$rt;
                    $discount=$rs->discount."%";
                }else{
                    $total=$rs->price-$rs->discount;
                    $discount=number_format($rs->discount);
                }
            }else{
                $total=$rs->price;
                $discount="ไม่มีส่วนลด";
            }
            $slip = array();
            $slip[] = action_custom(site_url("course/payment/getSlip/{$rs->course_registerId}"),'btn-primary various5 fancybox.iframe','add ','สลิป','fa-file','');
            if($rs->isPaypal=='1'){ $p='PayPal'; }else if($rs->isPaypal=='2'){ $p='Omise'; }else{ $p=implode($slip," ");}

            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['code'] = $rs->code.$st;
            $column[$key]['title'] = $rs->title;
            $column[$key]['member'] = $rs->firstname.' '.$rs->lastname;
            $column[$key]['price'] = number_format($rs->price);
            $column[$key]['couponCode'] = $rs->couponCode;
            $column[$key]['discount'] = $discount;
            $column[$key]['total'] = number_format($total);
            $column[$key]['slip'] = $p;
            $column[$key]['active'] = $statusPicker;
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            //$column[$key]['action'] = "";//Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->order_list_m->insert($value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['course_registerId'] = $id;
        $info = $this->order_list_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->order_list_m->update($id, $value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['title'] = $input['title'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }    
    
    
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->order_list_m->get_rows($input);
        $infoCount = $this->order_list_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_registerId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->order_list_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->order_list_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->order_list_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->order_list_m->update_in($input['id'], $value);
            } 

            if ( $type == "status" ) {
                $value['status'] = $input['status'];
                $result = $this->order_list_m->update_in($input['id'], $value);

                $this->update_course($input['id'][0],$value['status']);
                
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function update_course($course_registerId,$status)
    {
        $input['course_registerId'] = $course_registerId;
        //$info = $this->order_list_m->get_rows($input)->row();


        $course_register=$this->course_m->get_CourseRegis2($course_registerId)->row();

       
        $input_p['promotionId'] = $course_register->promotionId;
        $info['promotion']=$this->course_m->get_promotion_rows2($input_p)->row_array();

        $courseId  = array();
        $courseId[]  = $course_register->courseId;
        $promotionContent=$this->course_m->get_promotion_content($info['promotion']['promotionId'])->result();
        if(!empty($promotionContent)){ 
            foreach ($promotionContent as $key => $value_) {
                 $courseId[]  = $value_->courseId;
            }
        }
 //arr($courseId);exit();
        $this->order_list_m->del_course_member($course_register->code);
        if($status==1){

            foreach ($courseId as $key => $value) {
                //$this->order_list_m->plus_learner($value);
                $value_i['courseId']=$value;
                $value_i['userId']=$course_register->userId;
                $value_i['code']=$course_register->code;
                $value_i['active']=1;
                $value_i['recycle']=0;
                $value_i['createDate'] = db_datetime_now();
                $this->order_list_m->insert_course_member($value_i);
            }
        }
        
    }
    
}
