<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stat_admin_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.firstname, b.lastname')
                        ->select('c.title')
                        ->from('usertracking a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        ->join('module c', 'a.class = c.class', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('usertracking a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        ->join('module c', 'a.class = c.class', 'left')
                        ->count_all_results();
        return $query;
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.requestUri', $param['keyword'])
                    ->or_like('b.firstname', $param['keyword'])
                    ->or_like('b.lastname', $param['keyword'])
                    ->or_like('c.title', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $param['createStartDate'] = strtotime($param['createStartDate']." 00:00");
            $param['createEndDate'] = strtotime($param['createEndDate']." 23:59");
            $this->db->where("a.timestamp BETWEEN {$param['createStartDate']} AND {$param['createEndDate']}");
        }        
        // END form filter
        
        if ( isset($param['search']['value']) && ($param['search']['value'] != "") ) {
            $this->db
                    ->group_start()
                    ->like('a.requestUri', $param['search']['value'])
                    ->or_like('b.firstname', $param['search']['value'])
                    ->or_like('b.lastname', $param['search']['value'])
                    ->or_like('c.title', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.timestamp";
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.firstname";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "c.title";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.requestUri";
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.ip";
                if ($param['order'][0]['column'] == 6) $columnOrder = "a.browser";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycleDate";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['recycle'])) {
            $this->db->where('a.recycle', $param['recycle']);
        }

    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('id', $id)
                        ->update('usertracking', $value);
        return $query;
    }     

}
