<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_history_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    public function get_year_dd() {
        $info = $this->db
                ->select("DISTINCT(DATE_FORMAT(createDate, '%Y')) year")
                ->where('type', 'front')
                ->order_by('year')
                ->get('track')
                ->result_array();
        $temp = array();
        foreach ($info as $rs)
            $temp[$rs['year']] = $rs['year'];
        return $temp;
    }

    public function get_month_dd() {
        $month = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        unset($month[0]);
        return $month;
    }
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                         ->select('b.firstname, b.lastname')
                        ->from('track a')
                        ->join('user b', 'a.memberId = b.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                         ->select('b.firstname, b.lastname')
                        ->from('track a')
                        ->join('user b', 'a.memberId = b.userId', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.firstname', $param['keyword'])
                    ->or_like('b.lastname', $param['keyword'])
                    ->or_like('a.ip', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
                
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
             $this->db
                    ->group_start()
                    ->like('b.firstname', $param['keyword'])
                    ->or_like('b.lastname', $param['keyword'])
                    ->or_like('a.ip', $param['keyword'])
                    ->group_end();
        }
              $this->db
                     ->order_by('a.createDate', 'DESC');

        // if ( isset($param['order']) ){
        //     if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
        //     if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
        //     if ( $this->router->method =="data_index" ) {
        //         if ($param['order'][0]['column'] == 3) $columnOrder = "a.createDate";
        //         if ($param['order'][0]['column'] == 4) $columnOrder = "a.updateDate";
        //     } else if ( $this->router->method =="data_trash" ) {
        //         if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycleDate";
        //     }
        //     $this->db
        //             ->order_by($columnOrder, $param['order'][0]['dir']);
        // } 
        
        // if ( isset($param['repoId']) ) 
        //     $this->db->where('a.repoId', $param['repoId']);

        // if ( isset($param['recycle']) )
        //     $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('repo', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('repoId', $id)
                        ->update('repo', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('repoId', $id)
                        ->update('repo', $value);
        return $query;
    }    

}
