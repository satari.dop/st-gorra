<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sidebar_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    public function get_sidebar() {
//        $this->db->cache_on();
        if ( $this->session->user['type'] != "developer" ) {
            $this->db
                    ->where('isDev', 0);
        }
        $query = $this->db
                        ->from('module')
                        ->where('active', 1)
                        ->where('isSidebar', 1)
                        ->order_by('parentId', 'asc')
                        ->order_by('order', 'asc')
                        ->get();
//        $this->db->cache_off();
        return $query;
    }
    
    public function get_all_child($table="sidebar", $categoryId="", $parentField="parentId", $parentId=0 )
    {
        $sql = "SELECT GROUP_CONCAT(lv SEPARATOR ',') AS allChild FROM (
            SELECT @pv:=(SELECT GROUP_CONCAT({$categoryId} SEPARATOR ',') FROM {$table} WHERE
            FIND_IN_SET({$parentField}, @pv)) AS lv FROM {$table}
            JOIN (SELECT @pv:=?)tmp WHERE {$categoryId} IN (@pv)) a;";
        $query = $this->db
                        ->query($sql, array($parentId))
                        ->row_array();
        if ($query['allChild'] == "") {
           $category = $parentId;
        } else {
           $category = $query['allChild'].','.$parentId; 
        }
        $category = explode(',', $category);
        return $category;
    }    
    
    public function get_all_parent($currentId=0, $table="module", $currentField="moduleId", $parentField="parentId" )
    {
//        $this->db->cache_on();
         $sql = "SELECT GROUP_CONCAT(T2.{$currentField} SEPARATOR ',') AS allParent
                FROM (
                    SELECT
                         @r AS _id,
                         (SELECT @r := {$parentField} FROM {$table} WHERE {$currentField} = _id) AS parent_id,
                         @l := @l + 1 AS lvl
                     FROM
                        (SELECT @r := ?, @l := 0) vars,
                        {$table} h
                     WHERE @r <> 0) T1
                JOIN {$table} T2
                ON T1._id = T2.{$currentField}
                 ORDER BY T1.lvl DESC";
         $query = $this->db
                        ->query($sql, array($currentId))
                         ->row_array();
        $allParent = explode(',', $query['allParent']);
 //        $this->db->cache_off();
         return $allParent;
    }
    
    public function get_module(){
        $query = $this->db
                        ->select('moduleId, parentId, title')
                        ->where('active', 1)
                        ->where('recycle', 0)
                        ->where('isSidebar', 1)
                        ->order_by('parentId', 'asc')
                        ->order_by('order', 'asc')                
                        ->get('module');
        return $query;
    }

}
