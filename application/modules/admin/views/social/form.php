<div class="box">

    <div class="box-header with-border">

        <h3 class="box-title">แบบฟอร์ม</h3>

    </div>

    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>

    <div class="box-body">    

        <h4>ข้อมูลทั่วไป</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพโซเชียล</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span><!-- ไฟล์ภาพ : jpg , jpeg , png , gif / --> 20px*20px (กว้าง x สูง) </span>
                    </div>

                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                    
                     
                </div>
                
                
            </div>
        </div>
        <div class="form-group">

            <label class="col-sm-2 control-label">ชื่อโซเชียล</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>

            </div>

        </div>
        <div class="form-group hidden">

            <label class="col-sm-2 control-label">หัวข้อแสดงหน้าเว็บไซต์</label>

            <div class="col-sm-7">

               <input  type="text" class="form-control" name="excerpt" value="<?php echo isset($info->excerpt) ? $info->excerpt : NULL ?>">

            </div>

        </div>
        <div class="form-group">

            <label class="col-sm-2 control-label">ลิงค์เชื่อมโยง</label>

            <div class="col-sm-7" >
               <input  name="link" type="text" class="form-control"  value="<?php echo isset($info->link) ? $info->link : NULL ?>">
            </div> 

        </div>
        
        <div class="form-group">

            <label class="col-sm-2 control-label">ช่วงวันที่แสดงผล</label>

            <div class="col-sm-3">

               <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>"/>

                    <input type="hidden" name="startDate"  value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>"/>

                    <input type="hidden" name="endDate"  value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>"/>

            </div>

        </div>
        

        

        

    <div class="box-footer">

        <div class="col-sm-2">

        </div>

        <div class="col-sm-7">

            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 

        </div>

    </div>

    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">

    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->bannerId) ? encode_id($info->bannerId) : 0 ?>">

    <?php echo form_close() ?>

</div>

<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
            
            <!-- <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button> -->
        </div>
    </div>
    {% } %}
</script>
