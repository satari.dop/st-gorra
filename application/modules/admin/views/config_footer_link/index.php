<div class="box">
    
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        
        <div class="form-group">
            <div class="col-sm-10 col-md-offset-1">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_0" data-toggle="tab" aria-expanded="true"><i class="fa fa-money"></i> นโยบายการคืนเงิน</a></li>
                        <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> แนะนำการใช้งาน</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa  fa-chain"></i> ข้อกำหนดและเงื่อนไงในการใช้บริการ</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-large"></i> วิธีการสมัคร</a></li>
                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="true"><i class="fa fa-file-text"></i> นโยบายความเป็นส่วนตัว</a></li>
                        
                    </ul>

                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="tab_0">
                            <div class="row"  style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                   
                                    <textarea name="refund_policy" rows="3" class="form-control" id="refund_policy" ><?php echo isset($info['refund_policy']) ? $info['refund_policy'] : NULL ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1">
                            
                           <div class="row"  style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <?php ///echo $this->ckeditor->editor("recommended_usage", isset($info['recommended_usage']) ? $info['recommended_usage'] : NULL, "normal", 350); ?>
                                     <textarea name="recommended_usage" rows="3" class="form-control" id="recommended_usage" ><?php echo isset($info['recommended_usage']) ? $info['recommended_usage'] : NULL ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="row"  style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <?php //echo $this->ckeditor->editor("conditions", isset($info['conditions']) ? $info['conditions'] : NULL, "normal", 350); ?>
                                     <textarea name="conditions" rows="3" class="form-control summernote" id="conditions" ><?php echo isset($info['conditions']) ? $info['conditions'] : NULL ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="row"  style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <?php //echo $this->ckeditor->editor("how_to_apply", isset($info['how_to_apply']) ? $info['how_to_apply'] : NULL, "normal", 350); ?>
                                     <textarea name="how_to_apply" rows="3" class="form-control summernote" id="how_to_apply" ><?php echo isset($info['how_to_apply']) ? $info['how_to_apply'] : NULL ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_4">
                           <div class="row"  style="margin-bottom: 15px; padding:0px 15px">
                                <div class="col-sm-12">
                                    <?php //echo $this->ckeditor->editor("privacy_policy", isset($info['privacy_policy']) ? $info['privacy_policy'] : NULL, "normal", 350); ?>
                                     <textarea name="privacy_policy" rows="3" class="form-control summernote" id="privacy_policy" ><?php echo isset($info['privacy_policy']) ? $info['privacy_policy'] : NULL ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        
    </div>
    <div class="box-footer">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>