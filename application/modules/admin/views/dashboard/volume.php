<section class="row" >

</section>

<div class="row ">
    <div class="col-md-10">
        <section class="row" >
            <div class="col-md-12">
                <div class="box box-solid bg-black-gradient">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ผลิตการเกษตร</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-black btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_volumn"></div>
                            </div>                         
                        </div>                 
                    </div>
                </div>
            </div>            
            <div class="col-md-6">
                <div class="box box-solid bg-green-gradient">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ผลผลิตการเกษตรปี 2561</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-green btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_map_volumn" style="height:500px"></div>
                            </div>                           
                        </div>                 
                    </div>
                </div>
            </div>       
            <div class="col-md-6">
                <div class="box box-solid bg-red-gradient">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ผลผลิตการเกษตรปี 2561</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-red btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_pie" style="height:500px"></div>
                            </div>                           
                        </div>                 
                    </div>
                </div>
            </div>                  
        </section>
    </div>
    
    <div class="col-md-2">
        <div class="affix-top">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-success">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
                                                <i class="fa fa-align-left"></i> พืชผลสำคัญ
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                                        <ul>
                                            <li>ถั่วเหลือง</li>
                                            <li>ข้าว</li>
                                            <li>มันสำปะหลัง</li>
                                            <li>ข้าวโพด</li>
                                            <li>กระเทียม</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel box box-danger">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                                                <i class="fa fa-align-left"></i> ปศุสัตว์
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
                                        <ul>
                                            <li>สุกร</li>
                                            <li>ไก่</li>
                                            <li>ไข่ไก่</li>
                                            <li>โคเนื้อ</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel box box-info">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="" aria-expanded="true">
                                                <i class="fa fa-align-left"></i> ประมง
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" style="">
                                        <ul>
                                            <li>กุ้งขาวแวนนา</li>
                                        </ul>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <p><b>ข้อมูลปริมาณการผลิต</b><br>สินค้าเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="<?php echo site_url('admin/dashboard/volume'); ?>" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <p><b>ข้อมูลราคา</b><br>สินค้าเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-maroon">
                        <div class="inner">
                            <p><b>ข้อมูลต้นทุนการผลิต</b><br>สินค้าเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>   
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <p><b>ข้อมูลการนำเข้าส่งออก</b><br>สินค้าเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>  
                </div>
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <p>ปฏิทินการเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>                      
                </div>
                <div class="col-md-12 col-xs-6">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <p>ปฏิทินการเกษตร</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>                        
                </div>

            </div>




        </div>
    </div>    

</div>






