<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
         <div class="form-group">
            <label class="col-sm-3 control-label">รหัสคูปอง</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info->couponCode) ? $info->couponCode : $couponCode ?>" type="text" id="input-couponCode" class="form-control" name="couponCode" readonly >
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">ชื่อคูปอง</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>
       

         <div class="form-group">
            <label class="col-sm-3 control-label" for="title">จำนวนคอร์ส/คน</label>
            <div class="col-sm-2">
               <select class="select2" name="isCourse" id="isCourse">
                   <option value="0" <?php if(!empty($info) && $info->isCourse==0){ echo "selected"; }?>>ไม่จำกัด</option>
                   <option value="1" <?php if(!empty($info) && $info->isCourse==1){ echo "selected"; }?>>จำกัด</option>
               </select>
               
            </div>
            
            <div class="col-sm-1" style="display: none;" id="isCourseNum-d">
              
               <input placeholder="ระบุ" value="<?php echo isset($info->isCourseNum) ? $info->isCourseNum : NULL ?>" type="number" id="input-title" class="form-control" name="isCourseNum" >
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-3 control-label" for="title">จำนวนคนลงทะเบียน</label>
            <div class="col-sm-2">
                <select class="select2" name="isMember" id="isMember">
                   <option value="0" <?php if(!empty($info) && $info->isMember==0){ echo "selected"; }?>>ไม่จำกัด</option>
                   <option value="1" <?php if(!empty($info) && $info->isMember==1){ echo "selected"; }?>>จำกัด</option>
               </select>
               
            </div>
            
            <div class="col-sm-1" style="display: none;" id="isMemberNum-d">
              
               <input placeholder="ระบุ"  value="<?php echo isset($info->isMemberNum) ? $info->isMemberNum : NULL ?>" type="number" id="input-title" class="form-control" name="isMemberNum" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="title">ประเภทส่วนลด</label>
            <div class="col-sm-7">
                <label class="icheck-inline"><input id="display1"  type="radio" name="type" value="1" class="icheck" <?php if(isset($info->type) && $info->type=="1"){ echo "checked"; }?> /> บาท</label>
                <label class="icheck-inline"><input id="display2" type="radio" name="type" value="2" class="icheck" <?php if(isset($info->type) && $info->type=="2"){ echo "checked"; }?>/> ส่วนลด %</label>
               
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">จำนวนส่วนลด</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info->discount) ? number_format($info->discount) : NULL ?>" type="text" id="input-discount" class="amount form-control" name="discount" >
                <div id="alert_e" style="color: red;"></div>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-3 control-label">ช่วงเวลาคูปอง</label>
            <div class="col-sm-3">
                    <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                    <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                    <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
            </div>
        </div>  

        
    </div>
    <div class="box-footer">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->couponId) ? encode_id($info->couponId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="coverImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="contentImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="galleryImageTitle[]" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

