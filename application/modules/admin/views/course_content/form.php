<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
               
        <div class="form-group">
            <label class="col-sm-2 control-label">สถานะ</label>
            <div class="col-sm-7 icheck-inline">
                <label><input <?php echo $info['active']==1 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                <label><input <?php echo $info['active']==0 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
            </div>
        </div> 
         
       
        <div class="form-group">
            <label class="col-sm-2 control-label">เนื้อหาหลัก</label>
            <div class="col-sm-7">
                <?php echo $parentModule ?>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label"><font color=red>*</font> ชื่อเนื้อหา</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['title']) ? $info['title'] : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>    
       <div id="c-content" style="display: none;">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="descript">อธิบาย</label>
                <div class="col-sm-7">
                  <!--   <textarea name="descript" rows="3" class="form-control"><?php echo isset($info['descript']) ? $info['descript'] : "" ?></textarea> -->
                      <?php echo $this->ckeditor->editor("descript", isset($info['descript']) ? $info['descript'] : NULL, "normal", 200); ?>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-sm-2 control-label"><font color=red>*</font> ประเภทเนื้อหา</label>
                <div class="col-sm-4">
                     <?php $type = array(""=>'เลือก',0=>'ตัวอย่างดูฟรี', 1=>'ลงทะเบียน') ?>
                    <?php echo form_dropdown('type', $type,  isset($info['type']) ? $info['type'] : NULL ,  'class="form-control select2" required') ?>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-sm-2 control-label">ลิงก์วิดีโอ</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info['videoLink']) ? $info['videoLink'] : NULL ?>" type="text" id="input-videoLink" class="form-control" name="videoLink" required>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-sm-2 control-label">ความยาววิดีโอ</label>
                <div class="col-sm-2">
                    <input value="<?php echo isset($info['videoLength']) ? $info['videoLength'] : NULL ?>" type="text" id="input-videoLength" class="form-control" name="videoLength" required>
                </div>
                 <span class="col-sm-2 text-left">
                    นาที
                </span>
            </div> 

            <div class="form-group">
                <label class="col-sm-2 control-label">ไฟล์เอกสาร</label>
                <div class="col-sm-7">
                    <div class="input-group">
                    

                        <input name="fileTypeUploadType" id="rd_upload_type_system" value="1" type="radio"   onclick="displayUploadType(this)" <?php echo (!empty($info) && $info['fileTypeUploadType'] == 1) ? "checked='checked'" : ""; ?>  <?php echo isset($info['fileTypeUploadType']) ? NULL  : "checked='checked'" ?>/> อัพโหลดผ่านหน้าเว็บ
                        <input name="fileTypeUploadType" id="rd_upload_type_record" value="2" type="radio" onclick="displayUploadType(this)" <?php echo (! empty($info) && $info['fileTypeUploadType'] == 2) ? "checked='checked'" : ""; ?> /> URL         
                     
                    </div>
                </div>
            </div>  

            <div class="form-group" id="f-file" >
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <div class="row">
                        <!--  -->
                        <div class="col-sm-6" style="margin-bottom: 10px">
                             <!-- image-preview-filename input [CUT FROM HERE]-->
                            <div class="input-group">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">อัพโหลด</span>
                                        <input type="file"  name="input-file-preview"/> <!-- rename it -->
                                    </div>
                                </span>


                            </div>
                           
                           <!-- /input-group image-preview [TO HERE]--> 
                        </div>
                        <div class="col-sm-6">
                             <span></span>
                        </div>

                        <div class="col-sm-12"  id="doc-attach" style="padding-left: 0px;">
                             <script> var dataDocAttach = <?php echo isset($docAttach) ? json_encode($docAttach) : "{}" ?></script>
                        </div>
                        
                         
                    </div>
                </div>
            </div> 

            <div class="form-group" id="f-url" style="display: none">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                     <input value="<?php echo isset($info['fileUrl']) ? $info['fileUrl'] : NULL ?>" type="text" id="input-fileUrl" class="form-control" name="fileUrl" >
                </div>
            </div> 


        </div>

    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="courseId" id="input-courseId" value="<?php echo $courseId ?>">
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['course_contentId']) ? encode_id($info['course_contentId']) : 0 ?>">
    <?php echo form_close() ?>
</div>


<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
    </div>
   
    <div class="col-sm-2" id="action">
        <div class="btn-group">
           
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>