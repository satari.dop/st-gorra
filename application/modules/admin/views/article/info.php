<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">เนื้อหา</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        <h4 class="block">ข้อมูลพื้นฐาน</h4>           
        <div class="form-group">
            <label class="col-sm-2 control-label">เนื้อหา</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="content"><?php echo isset($info['content']) ? $info['content'] : NULL ?></textarea>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>