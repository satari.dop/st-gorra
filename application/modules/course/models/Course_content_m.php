<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Course_content_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_list($param)
    {
        $this->_condition($param);
    
        $query = $this->db
                        ->select('a.*')
                        ->from('course_content a')
                        ->get();

        return $query;
    }

     private function _condition($param) 

    {   
        
        if ( isset($param['active']) ) {
            $this->db->where('a.active', $param['active']);
        }   
 

        if ( isset($param['parentId']) && $param['parentId']!="") {
             $this->db->where('a.parentId', $param['parentId']);
        } else{
             $this->db->where('a.parentId', 0);
        }
        if ( isset($param['courseId']) )
            $this->db->where('a.courseId', $param['courseId']);  

        if ( isset($param['course_contentId']) )
            $this->db->where('a.course_contentId', $param['course_contentId']);      

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

         $this->db->order_by('a.order', 'asc');


    }

    public function get_list_byid($param)
    {
        $this->_condition_byid($param);
    
        $query = $this->db
                        ->select('a.*,b.order as order1')
                        ->from('course_content a')
                        ->join('course_content b', 'a.parentId = b.course_contentId', 'left')
                        ->get();

        return $query;
    }

     private function _condition_byid($param) 

    {   
        
        if ( isset($param['active']) ) {
            $this->db->where('a.active', $param['active']);
        }   
 

        
        if ( isset($param['courseId']) )
            $this->db->where('a.courseId', $param['courseId']); 

        if ( isset($param['parentId']) )
            $this->db->where('a.parentId !=', 0);   

        if ( isset($param['course_contentId']) && $param['course_contentId']!=0 )
            $this->db->where('a.course_contentId', $param['course_contentId']);      

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

        $this->db->order_by('b.order', 'asc');
        $this->db->order_by('a.order', 'asc');


    }

   
        
}