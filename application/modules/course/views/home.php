<section class="section blog-area">
		<div class="container">
			<div class="row">
				

				<div class="col-lg-12 col-md-12">
					<div class="blog-posts">
						<div class="title">
							<h3>คอร์สเรียนออนไลน์</h3>
							<!-- <div class="separator"></div> -->
						</div>
						<div class="row">

							<?php foreach ($course_home as $key => $rs) :?>

							<div class="col-xl-4 col-lg-6 col-md-6 col-ms-12 blog-item">
								<div class="single-post ">

									<a href="<?php echo site_url("course/detail/{$rs->linkId}");?>">
									<div class="hover13 column">
									    <figure>
									    	<img src="<?php echo $rs->image ?>" alt="Blog Image">
									    </figure>
									</div>
								    </a>

								
									<div class="title-blog"><h5><a href="<?php echo site_url("course/detail/{$rs->linkId}");?>"><b class="light-color"><?php echo $rs->title ?></b></a></h5></div>
									<div class="separator-3"></div>
									<span class="learner menu-icon"> <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-member-w.svg"  /></span><span class="learner-2" > <?php echo $rs->learner2 ?></span>
								   <span class="rating4">
								   	<?php echo $rs->stars ?>
								   	    
								   </span>
                        
                       
									<div class="separator-3"></div>
									<?php if($isLogin && !empty($rs->info_user)){ ?>

									<div style="padding-top:35px"></div>
										
									</span>
									<?php }else{ ?>
									<span class="btn-price btn-price-<?php echo $rs->courseId ?> "><?php echo number_format($rs->price) ?> ฿</span> 
									<span class="btn-price-pro" id="btn-price-pro-<?php echo $rs->courseId ?>" style="display: none;">

										<?php 
										if($rs->promotion['type']==2){
											echo "จ่ายเท่าไรก็ได้";
										}else{
											if($rs->promotion['discount']!=0 ){  
												echo number_format($rs->promotion['discount']) ?> ฿ <?php 
											}else{ 
												echo "โปรแถม"; 
											}
										}
										?>
											
									</span>
								   <?php } ?>
									<div class="product-<?php echo $rs->courseId ?> promotion-content">

										
											<?php if($isLogin && !empty($rs->info_user)){ ?>
                                              <span class="time"></span>
                                              <p>
                                              	<span class="cart cart-icon">
                                              	</span>
												<a  class="button-click" href="<?php echo site_url("course/list_video/{$rs->linkId}");?>" style='margin-top: 32px;'><span style="color: #f7f007" >
												คลิก!! เริ่มเรียน </span><img src="<?php echo base_url('assets/website/images/icon/click.png')?>" style="width: 30px">
											    </a>
											</p>
											<?php }else{ ?>
												<span class="time"><span id="showRemain-<?php echo $rs->courseId ?>"></span></span>
											<p>
											<span class="cart cart-icon">
												<a <?php if(!$isLogin){ ?> href="javascript:void(0)" class="modalLogin" 
												<?php }else{ ?>
													class="various5 fancybox.iframe" href="<?php echo site_url('course/payment/register/'.$rs->courseId);?>"
												<?php } ?>>
												<img src="<?php echo base_url("assets/website/") ?>images/icon/icon-car.png" style="width: 40px;height: 40px;"  />
												<!-- <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="cart-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M28.6,27.53a3.88,3.88,0,1,1-2.74,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM10.72,10.68H35.64a.89.89,0,0,1,.89.89V12a.88.88,0,0,1-.89.88H11.31l1.12,4.18H33.65a.89.89,0,0,1,.89.89v.44a.89.89,0,0,1-.89.89H13l.32,1.2A3.28,3.28,0,0,0,16.57,23H32.08a.88.88,0,0,1,.89.88v.45a.89.89,0,0,1-.89.89H16.57a5.52,5.52,0,0,1-5.37-4.12L8.1,9.49A3.28,3.28,0,0,0,4.88,7h-3A.89.89,0,0,1,1,6.14V5.69a.89.89,0,0,1,.89-.89h3A5.58,5.58,0,0,1,8.27,5.94a5.48,5.48,0,0,1,2,3l.48,1.76Zm6.14,16.85a3.88,3.88,0,1,1-2.73,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM18,30.23a1.68,1.68,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48,1.66,1.66,0,0,0-.48,1.17,1.65,1.65,0,1,0,3.3,0A1.66,1.66,0,0,0,18,30.23Zm11.74,0a1.7,1.7,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48A1.66,1.66,0,0,0,27,31.4a1.64,1.64,0,0,0,1.65,1.65,1.66,1.66,0,0,0,1.17-.48,1.64,1.64,0,0,0,.48-1.17,1.66,1.66,0,0,0-.48-1.17Z"></path></svg> -->
												</a>
											</span>
											
											
												<a  class="button-click" href="<?php echo site_url("course/detail/{$rs->linkId}");?>"><span style="color: #f7f007">
												คลิก!! ดูรายละเอียด </span><img src="<?php echo base_url('assets/website/images/icon/click.png')?>" style="width: 30px">
											    </a>
											 </p>
											<?php } ?>
											
										
										
										
									</div>
									
								</div><!-- single-post -->
							</div><!-- col-sm-6 -->

							  <?php endforeach; ?> 

							

						</div><!-- row -->

						

					</div><!-- blog-posts -->
				</div><!-- col-lg-4 -->


				

			</div><!-- row -->
		</div><!-- container -->
	</section><!-- section -->