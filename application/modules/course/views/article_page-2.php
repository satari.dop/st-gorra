                            <?php foreach ($course_home as $key => $rs) :?>

							<div class="col-xl-12 col-lg-6 col-md-6 col-sm-6 blog-item-2">
								<div class="single-post ">

									
									<div class="hover13 column">
										<a href="<?php echo site_url("course/detail/{$rs->linkId}");?>">
										    <figure>
										    	<img src="<?php echo $rs->image ?>" alt="Blog Image">
										    </figure>
										</a>
									</div>

								
									<div class="title-blog"><h5><a href="<?php echo site_url("course/detail/{$rs->linkId}");?>"><b class="light-color"><?php echo $rs->title ?></b></a></h5></div>
									<div class="separator-3"></div>
									<span><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="learner menu-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M20.31,6A8,8,0,0,1,26,8.38,7.75,7.75,0,0,1,27.68,11a7.93,7.93,0,0,1,0,6.1A7.79,7.79,0,0,1,26,19.67a8.06,8.06,0,0,1-2.59,1.73,8.05,8.05,0,0,1-6.1,0,8.06,8.06,0,0,1-2.59-1.73,7.84,7.84,0,0,1-1.73-2.6,7.91,7.91,0,0,1,0-6.09,7.84,7.84,0,0,1,1.73-2.6A8,8,0,0,1,20.31,6Zm2.23,2.6a5.87,5.87,0,0,0-4.46,0A5.75,5.75,0,0,0,16.19,9.9a5.85,5.85,0,0,0-1.27,6.36,6,6,0,0,0,1.27,1.89,5.75,5.75,0,0,0,1.89,1.26,5.84,5.84,0,0,0,6.35-1.26,5.83,5.83,0,0,0,0-8.25,5.75,5.75,0,0,0-1.89-1.26Zm.95,13.85a11.65,11.65,0,0,1,2.39,1A12.38,12.38,0,0,1,28,25a11.47,11.47,0,0,1,2.29,2.71,10.88,10.88,0,0,1,1.22,3,3.1,3.1,0,0,1-.57,2.69h0a3,3,0,0,1-1.1.89,3.08,3.08,0,0,1-1.38.32H12.17a3.16,3.16,0,0,1-2.48-1.21,3.18,3.18,0,0,1-.6-1.29,3.06,3.06,0,0,1,0-1.41,11.17,11.17,0,0,1,1.21-3A11.47,11.47,0,0,1,12.63,25a12.38,12.38,0,0,1,2.11-1.49,11.65,11.65,0,0,1,2.39-1l.85-.24.4.8,1.93,3.84,1.93-3.84.4-.8.85.24Zm1.36,2.87a9.33,9.33,0,0,0-1.11-.52l-2.47,4.92-1,1.91-1-1.91-2.47-4.92a8.52,8.52,0,0,0-1.1.52,9.24,9.24,0,0,0-1.73,1.21,9,9,0,0,0-1.86,2.2,8.85,8.85,0,0,0-1,2.44,1,1,0,0,0,0,.45,1,1,0,0,0,.2.41.91.91,0,0,0,.34.28.87.87,0,0,0,.44.1H28.45a.89.89,0,0,0,.44-.1.81.81,0,0,0,.34-.28,1,1,0,0,0,.2-.41,1,1,0,0,0,0-.45,8.85,8.85,0,0,0-1-2.44,9,9,0,0,0-1.86-2.2,9.5,9.5,0,0,0-1.72-1.21Z"></path></svg></span><span class="learner-2"> <?php echo $rs->learner ?></span>
									<div class="separator-3"></div>
									<span class="btn-price btn-price-<?php echo $rs->courseId ?> "><?php echo number_format($rs->price) ?> ฿</span> <span class="btn-price-pro" id="btn-price-pro-<?php echo $rs->courseId ?>" style="display: none;"><?php if($rs->promotion['discount']!=0){  echo number_format($rs->promotion['discount']) ?> ฿ <?php }else{ echo "โปรแถม";}?></span>
									<div class="product-<?php echo $rs->courseId ?> ">
										<span class="time"><span id="showRemain-<?php echo $rs->courseId ?>"></span></span>
											<p>
											<span class="cart"><a href="javascript:void(0)" <?php if(!$isLogin){ ?> class="modalLogin" 
												<?php }else{ ?>
													onclick="javascript:registerCourse(<?=$rs->courseId?>)"
												<?php } ?>><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 40 40" class="cart-icon svg replaced-svg"><defs><style>.cls-1{fill:#b6b6b7;}</style></defs><title>web</title><path class="cls-1" d="M28.6,27.53a3.88,3.88,0,1,1-2.74,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM10.72,10.68H35.64a.89.89,0,0,1,.89.89V12a.88.88,0,0,1-.89.88H11.31l1.12,4.18H33.65a.89.89,0,0,1,.89.89v.44a.89.89,0,0,1-.89.89H13l.32,1.2A3.28,3.28,0,0,0,16.57,23H32.08a.88.88,0,0,1,.89.88v.45a.89.89,0,0,1-.89.89H16.57a5.52,5.52,0,0,1-5.37-4.12L8.1,9.49A3.28,3.28,0,0,0,4.88,7h-3A.89.89,0,0,1,1,6.14V5.69a.89.89,0,0,1,.89-.89h3A5.58,5.58,0,0,1,8.27,5.94a5.48,5.48,0,0,1,2,3l.48,1.76Zm6.14,16.85a3.88,3.88,0,1,1-2.73,6.61h0a3.87,3.87,0,0,1,2.74-6.61ZM18,30.23a1.68,1.68,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48,1.66,1.66,0,0,0-.48,1.17,1.65,1.65,0,1,0,3.3,0A1.66,1.66,0,0,0,18,30.23Zm11.74,0a1.7,1.7,0,0,0-1.17-.48,1.64,1.64,0,0,0-1.17.48A1.66,1.66,0,0,0,27,31.4a1.64,1.64,0,0,0,1.65,1.65,1.66,1.66,0,0,0,1.17-.48,1.64,1.64,0,0,0,.48-1.17,1.66,1.66,0,0,0-.48-1.17Z"></path></svg></a></span>
											
											
												<a  class="button-click" href="<?php echo site_url("course/detail/{$rs->linkId}");?>"><span style="color: #f7f007">
												คลิก!! ดูรายละเอียด </span><img src="<?php echo base_url('assets/website/images/icon/click.png')?>" style="width: 30px">
											    </a>
											
											</p>
										
										
										
									</div>
									
								</div><!-- single-post -->
							</div><!-- col-sm-6 -->

							  <?php endforeach; ?> 