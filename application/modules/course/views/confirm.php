<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <base href="<?php echo site_url(); ?>">

<link rel="icon" href="<?php echo base_url('assets/website/') ?>images/favicon.ico" type="image/x-icon">

 

  <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.min.css' type='text/css' />

</head>
<body >

<script type='text/javascript' src='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.all.min.js'></script>  

<script type="text/javascript">
  
		swal({ 
		  title: "ยืนยันการชำระเงินสำเร็จ",
		  //text: "ผู้ใช้นี้ กำลังใช้งานอยู่ !",
		  type: "success" 
		}).then(function() {
		    window.location.href = "<?php echo site_url('admin/order_list');?>";
		})
	
</script>
</body>
</html>