<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Impexp_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        
       

       
    }

    public function getRows() 
    {
        $query = $this->orclDB->query("SELECT * FROM IMPEXP_PROD   ");
         return $query->result();
    }

    public function getQuantityOverviewReport() 
    {
       
        $query = $this->orclDB->query("SELECT
YR,
IMPEXP,
sum(QUANTITY) as a
FROM
IMPEXP_PROD
WHERE YR != '1999'
 GROUP BY YR,IMPEXP  ORDER BY YR ASC ");
         return $query->result();
    }

    public function getValueOverviewReport() 
    {
       
        $query = $this->orclDB->query("SELECT
YR,
IMPEXP,
sum(VALUE) as b
FROM
IMPEXP_PROD
WHERE YR != '1999' 
GROUP BY YR,IMPEXP ORDER BY YR ASC");
         return $query->result();
    }

}
