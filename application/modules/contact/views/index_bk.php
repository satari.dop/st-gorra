<section class="section blog-area">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6 col-md-6">
				<div class="blog-register">
					<div class="title">
						<h3>ติดต่อเรา</h3>
						<!-- <div class="separator"></div> -->
					</div>
					<div class="row">
						<?php echo form_open($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
							<ul class="form-style-1">
								 
							    <li><label>ชื่อ-นามสกุล <span class="required">*</span></label>
							    	<input type="text" name="fname" id="fname" class="field-divided" placeholder="ชื่อ" />&nbsp;<input type="text" name="lname" id="lname" class="field-divided" placeholder="นามสกุล" />
							    	<div class="alert_name"></div>
							    </li>
							    <li>
							        <label>Email <span class="required">*</span></label>
							        <input type="email" name="email" id="email" class="field-long" />
							        <div class="alert_email"></div>
							    </li>
							    <li>
							        <label>เบอร์โทรศัพท์ </label>
							        <input type="text" name="tel" class="field-long" />
							    </li>
							     <li>
							        <label>ข้อความ <span class="required">*</span></label>
							        <textarea name="massage" id="massage" class="field-long"  rows="3"></textarea>
							        <div class="alert_massage"></div>
							    </li>
							     <li>
							     	<input type="hidden" name="mode" value="create" />
							          <div id="html_element" style="margin-bottom: 10px"></div>
					                  <input type="hidden" name="robot"  class="form-control">
					                 <div id="form-success-div" class="text-success"></div> 
                                    <div id="form-error-div" class="text-danger"></div>
					             </li>
							    <li>
							        <button type="submit" class="btn btn-flat btn-register"><span id="form-img-div"></span> ส่งข้อความ</button>
							    </li>
							</ul>
						<?php echo form_close() ?>
					</div><!-- row -->
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			<div class="col-lg-6 col-md-6">
				<div class="title">
						<h3>ผ่านช่องทาง</h3>
						<!-- <div class="separator"></div> -->
				</div>
				<div class="contact-social">
						
						<!-- <a target="_blank" href="https://www.facebook.com/<?=$facebook;?>">
						 <img style="margin-bottom:6px;" src="<?php echo base_url("assets/website/images/facebook-3.png") ?>" alt="Line " ><span><h4> &nbsp;&nbsp;<?=$facebook;?></h4></span>
						</a>
						<br> -->
						<a href="https://m.me/<?php echo $facebook; ?>"><img src="<?php echo base_url("assets/website/images/messenger.png") ?>" style="width: 60px;"><span><h4>&nbsp;&nbsp;<?php echo $facebook; ?></h4></span></a>
						<br>
						<!-- <a href="javascript:void(0)" onclick="window.open('http://line.me/ti/p/~<?=$idLine;?>', '_blank');">
						  <img style="margin-bottom:6px;" src="<?php echo base_url("assets/website/images/line-3.png") ?>" alt="Line " ><span><h4> &nbsp;&nbsp;<?=$idLine;?></h4></span>
						</a>
						<br> -->
						<a href="tel:<?php echo $phoneNumber; ?>" style="padding-top: 10px;">
						   <img style="margin-bottom:6px;width: 60px;" src="<?php echo base_url("assets/website/images/phone-3.png") ?>" alt="Line "   ><span><h4>&nbsp;&nbsp;<?php echo $phoneNumber; ?> </h4></span>
						</a>
						
				</div>
			</div>
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->