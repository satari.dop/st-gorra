<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->library('encryption');
    }
    
    public function index()
    {

         $this->load->view('formLogin',$data); 
    }
    
    public function do_login()
    {
        $input = $this->input->post();
        $info = $this->login_m->by_username($input['username']);

        if ( empty($info) ) {
           

             $rs['info_txt'] = "error";
            $rs['text'] = "ไม่พบผู้ใช้งาน: {$input['username']}";
        } else {
            $password = md5($input['password']);
            if ( $password == $info['memberPassword'] ) {
                if ( $info['memberVerifyStatus'] == 0 ) {
                    

                    $rs['info_txt'] = "error";
                    $rs['text'] = "กรุณายืนยันการสมัครสมาชิกทางอีเมล์";
                } else if ( $info['memberActive'] == 0 ) {
                    

                    $rs['info_txt'] = "error";
                    $rs['text'] = "ถูกระงับการใช้งาน";
                } else {
                    
                    $rs['info_txt'] = "success";
                    $rs['text'] = "";
                    $this->_set_member($info);
                }
            } else {
                

                $rs['info_txt'] = "error";
                $rs['text'] = "รหัสผ่านไม่ถูกต้อง";
            }
            
        }   
       
            echo json_encode($rs);
            return false;
    }
    
    private function _set_member($member)
    {
        $value['memberId'] = $member['memberId'];
        $value['firstname'] = $member['memberName'];
        $value['lastname'] = $member['memberSurname'];
        $value['email'] = $member['memberEmail'];
        $value['username'] = $member['memberUsername'];
        $value['name'] = $member['memberName']." ".$member['memberSurname'];
        $value['memberFullName'] = $member['memberName'].' '.$member['memberSurname'];
        $this->session->set_userdata('member', $value);
        $this->session->set_flashdata('showMember','1');
        $value = array();
        $value['memberLastLogin'] = db_datetime_now();
        $this->login_m->update($value, $member['memberId']);
        return true;
    }
    
    
    public function _build_member($input)
    {
        $values = array();
        $dob = NULL;
        if ($input['bod']) {
            $bod = db_date($input['bod'][0].'-'.$input['bod'][1].'-'.$input['bod'][2]);
        }
        $values['firstname'] = html_escape($input['firstname']);
        $values['lastname'] = html_escape($input['lastname']);
        $values['email'] = $input['email'];
        $values['mobile'] = html_escape($input['mobile']);
        $values['gender'] = ($input['gender']);
        $values['occupation'] = html_escape($input['ocupation']);
        $values['country'] = html_escape($input['country']);
        $values['province'] = html_escape($input['province']);
        $values['district'] = html_escape($input['city']);
        $values['zip'] = html_escape($input['post_code']);
        $values['address'] = html_escape($input['address']);
        $values['bod'] = $bod;
        if ( $input['mode'] == 'create' ) {
            $values['createDate'] = db_datetime_now();  
        } else {
            $values['modifyDate'] = $values['lastLogin'] = db_datetime_now();
            $values['modifyBy'] = (int)$input['id'];
        }
        return($values);
    }


    public function check() {
        $input = $this->input->post();
        $info = $this->login_m->get_by_email($input);
        if (!empty($info)) {
            if ($info['verify'] == 1 || $info['type'] == 'developer') {
                if ($info['active'] == 1 && $info['policyActive'] == 1 || $info['type'] == 'developer') {
                    $pass = $this->encryption->decrypt($info['password']);
                    if ( $input['password'] == $pass ) {
                        $image = base_url('uploads/user.png');
                        $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                        if ( $upload->num_rows() != 0 ) {
                            $row = $upload->row();
                            if ( is_file("{$row->path}/{$row->filename}") )
                                $image = base_url("{$row->path}{$row->filename}");
                        }
                        $data['error'] = false;
                        $data['message'] = site_url();
                        $member['userId'] = $info['userId'];
                        $member['name'] = $info['firstname']." ".$info['lastname'];
                        $member['image'] = $image;
                        $member['email'] = $info['email'];
                        $member['sectionId'] = $info['sectionId'];
                        $member['partyId'] = $info['partyId'];
                        $member['positionId'] = $info['positionId'];
                        $member['degree'] = $info['degree'];
                        $member['type'] = $info['type'];
                        $member['policyId'] = $info['policyId'];
                        $member['isBackend'] = TRUE;
                        $this->session->set_userdata('member', $member);
                        $this->session->set_flashdata('firstTime', '1');
                        $this->login_m->update_last_login();
                    } else {
                        $data['error'] = true;
                        $data['message'] = "รหัสผ่านไม่ถูกต้อง";
                    }
                } else {
                    $data['error'] = true;
                    $data['message'] = "บัญชีของท่านถูกระงับการใช้งาน";
                }
            } else {
                $data['error'] = true;
                $data['message'] = 'กรุณายืนยันตัวตนอีเมล์ <a href="">ส่งอีกครั้ง</a>';
            }
        } else {
            $data['error'] = true;
            $data['message'] = 'ไม่พบบัญขีของท่านกรุณาลงทะเบียน';
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
            
    
}
