$(document).ready(function () {
	   $('.amount').keyup( function() {
	      //  alert('ff');
	        $(this ).val( formatAmount( $( this ).val() ) );
	        
	    });

		$('form#frm-save').submit(function(event) {
			if($('#price_all').val() < 3000){
		    
		      $(".alert_price_all").html('<font color=red>ยอดเงินสะสมปัจจุบันยังไม่ครบตามที่กำหนด</font>'); 
		      return false;
		    }
			if($('#bankCode').val()==""){
		      $('#bankCode').focus();
		      $(".alert_bankCode").html('<font color=red>* โปรดเลือกชื่อธนาคาร</font>'); 
		      return false;
		    }
		    if($('#price').val()==""){
		      $('#price').focus();
		      $(".alert_price").html('<font color=red>* กรอกจำนวนเงิน</font>'); 
		      return false;
		    }
		    if($('#bankName').val()==""){
		      $('#bankName').focus();
		      $(".alert_bankName").html('<font color=red>* กรอกชื่อบัญชี</font>'); 
		      return false;
		    }
		    if($('#bankNo').val()==""){
		      $('#bankNo').focus();
		      $(".alert_bankNo").html('<font color=red>* กรอกเลขที่บัญชี</font>'); 
		      return false;
		    }


		    if($('#fileBookbank').val()=="" && $('#fileBookbank_old').val()==""){
		      $('#fileBookbank').focus();
		      $(".alert_fileBookbank").html('<font color=red>* เลือกไฟล์</font>'); 
		      return false;
		    }
		    if($('#fileEvidence').val()=="" && $('#fileEvidence_old').val()==""){
		      $('#fileEvidence').focus();
		      $(".alert_fileEvidence").html('<font color=red>* เลือกไฟล์</font>'); 
		      return false;
		    }



		    $('#form-error-div').html(' ');
		    $('#form-success-div').html(' ');

		       // var conf = archiveFunction();
		        //alert(conf);
		        if(confirm("ยืนยันการแจ้งถอนเงินอีกครั้ง !!")){


		          event.preventDefault();             
		          var formObj = $(this);
		          var formURL = formObj.attr("action");
		          var formData = new FormData(this);
		          $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
		          $.ajax({
		            url: formURL,
		            type: 'POST',
		            data:formData,
		            dataType:"json",
		            mimeType:"multipart/form-data",
		            contentType: false,
		            cache: false,
		            processData:false,
		            success: function(data, textStatus, jqXHR){

		             if(data.info_txt == "error")
		             {
		               $('#form-img-div').html(' ');
		               $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
		               $("#form-error-div").slideDown(400);

		             }
		             if(data.info_txt == "success")
		             {
		              $('#form-img-div').html(' ');
		                    //$("#form-success-div").append('<p><strong><li class="text-green"></li>'+data.msg+'</strong>,'+data.msg2+'</p>');
		                    //$("#form-success-div").slideDown(400);

		                    // alert(data.msg);

		                    setInterval(function(){
		                     window.location=siteUrl+'user/profile/withdraw_success/'+data.result;
		                   },1000
		                   );
		                    // swal({ 
		                    //   title: data.msg,
		                    //   text: "",
		                    //   type: "success" 
		                    // }).then(function() {
		                    //     window.location.href = siteUrl;//'course/course_history';
		                    // })

		                  }

		                },
		                error: function(jqXHR, textStatus, errorThrown){
		                 $('#form-img-div').html(' ');
		                 $("#form-error-div").append("<p><strong><li class='text-red'></li>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
		                 $("#form-error-div").slideDown(400);
		               }

		             });
		        }else{
		          return false;
		        }

      });
});
function formatAmountNoDecimals( number ) {
  var rgx = /(\d+)(\d{3})/;
  while( rgx.test( number ) ) {
    var number = number.replace( rgx, '$1' + ',' + '$2' );
  }
  return number;
}

function formatAmount(str) {
//    
var number = str.toString();
var number = number.replace( /[^0-9]/g, '' );
//alert(number);
var num = number.split(',');
var num1 = num[0];
var num2 = num.length > 1 ? ',' + num[1] : '';

return formatAmountNoDecimals( num1 ) + num2;
}

function check_file_img(obj)
{
  var filename = obj.value;
  var valid_extensions = /(\.jpeg|\.jpg|\.png|\.gif|\.pdf)$/i;
  if(valid_extensions.test(filename)){
    return true;
  }else{
    alert('รูปแบบไฟล์ไม่ถูกต้อง !');
    obj.value='';
  }
}

function check_bank(obj)
{
  
 // alert(obj.value);
    var bankCode = obj.value;
    $.post(siteUrl+'/user/profile/withdraw_check_bank',{csrfToken: csrfToken,bankCode:bankCode},function(data){

        var res = JSON.parse(data);
        if(res.info_txt == "success")
        {
        	$('#bankName').val(res.result.bankName);
        	$('#bankNo').val(res.result.bankNo);
        	$('#fileBookbank_old').val(res.result.fileBookbank);
        	$('#fileEvidence_old').val(res.result.fileEvidence);

        	$('.alert_fileBookbank').html("<a style='color:#08c' target='_blank' href="+res.result.fileBookbank+">ไฟล์สำเนาหน้าบัญชี</a>");
        	$('.alert_fileEvidence').html("<a style='color:#08c' target='_blank' href="+res.result.fileEvidence+">สำเนาบัตรประชาชน</a>");
        }else{
        	$('#bankName').val("");
        	$('#bankNo').val("");
        	$('#fileBookbank_old').val("");
        	$('#fileEvidence_old').val("");

        	$('.alert_fileBookbank').html("");
        	$('.alert_fileEvidence').html("");

        }
    });
}