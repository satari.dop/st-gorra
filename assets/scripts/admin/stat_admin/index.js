"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie'), frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[1, "desc"]],
        pageLength: 25,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false, visible:true},
            {data: "timestamp", className: "", orderable: true},
            {data: "user", className: "", orderable: true},
            {data: "class", className: "", orderable: true},
            {data: "uri", className: "", orderable: true},
            {data: "ip", className: "", orderable: true},
            {data: "browser", className: "", orderable: true},
//            {data: "createDate", width: "130px", className: "", orderable: true},
//            {data: "updateDate", width: "130px", className: "", orderable: true},
//            {data: "active", width: "70px", className: "text-center", orderable: false},
//            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
