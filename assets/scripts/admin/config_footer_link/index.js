"use strict";
$(document).ready(function () {
   
   $('#refund_policy').summernote({
        fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 300,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile1(files[0]);
            }
        },

    });

   $('#recommended_usage').summernote({
        fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 300,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile2(files[0]);
            }
        },

    });
    
})

function sendFile1(file, editor, welEditable) {

        data = new FormData();
        data.append("file", file);
        data.append("csrfToken", get_cookie('csrfCookie'));
        $.ajax({
            data: data,
            type: 'POST',
            url: "admin/upload/sumernote_img_upload",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                $('#refund_policy').summernote('editor.insertImage', data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus+" "+errorThrown);
            }
        });
} 
function sendFile2(file, editor, welEditable) {

    data = new FormData();
    data.append("file", file);
    data.append("csrfToken", get_cookie('csrfCookie'));
    $.ajax({
        data: data,
        type: 'POST',
        url: "admin/upload/sumernote_img_upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
            $('#recommended_usage').summernote('editor.insertImage', data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
        }
    });
} 

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
