"use strict";
// Upload and image cropper
var dataTmpl = {}
var tmplName
var container
var objUploadId
var objUpload
var single
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 4 / 3,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj, select, tmpl){
   container = $(obj)
   tmplName = tmpl
   single = true
   if ( select === 'multiple' )
       single = false
}
var set_cropper = function(obj){
   objUpload = obj.parents('div#action').prevAll().eq(1).find('img')
   objUploadId = obj.parents('div#action').prevAll().eq(0).find('input#uploadId')
}
var remove_upload = function(obj){
    // if ( confirm('กรุณายืนยันการทำรายการ!') ) {
    //     obj.parents('div#action').prevAll().eq(1).remove()
    //     obj.parents('div#action').prevAll().eq(0).remove()
    //     obj.parents('div#action').remove()
    // } else {
    //     return false;
    // }


    bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
            obj.parents('div#action').prevAll().eq(1).remove()
            obj.parents('div#action').prevAll().eq(0).remove()
            obj.parents('div#action').remove()

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });
}


$(document).ready(function () {

	$("#cover-image").html(tmpl("tmpl-cover-image", dataCoverImage))
   

    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2')
    })
    
    $('input#filter-category').keyup(function () {
        var that = this, $allListElements = $('ul.filter-category > li');
        var $matchingListElements = $allListElements.filter(function (i, li) {
            var listItemText = $(li).text().toUpperCase(), searchText = that.value.toUpperCase();
            return ~listItemText.indexOf(searchText);
        });
        $allListElements.hide();
        $matchingListElements.show();
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
