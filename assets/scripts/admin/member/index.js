"use strict";
//  $('#data-list').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
//         // Ignore the Responsive control and checkbox columns
//     if ( $(this).hasClass( 'control' ) || $(this).hasClass('select-checkbox') ) {
//         return;
//     }

//     editor.inline( this );
// } );
$(document).ready(function () {

    //$("input[type='checkbox']").bootstrapToggle();
    
      var table = dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        responsive: true,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie'),frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[3, "desc"]],
        pageLength: 10,
        columns: [
            
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "name",width: "100px", className: "", orderable: true},
             {data: "username",width: "400px", className: "", orderable: false},
            // {data: "phone", className: "", orderable: true},
            // {data: "email", className: "", orderable: true},
            {data: "lastLogin", width: "100px", className: "", orderable: true},
            {data: "updateDate",width: "100px",  className: "", orderable: true},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ],"fnDrawCallback": function() {
            $(".toggle-demo").bootstrapToggle();
            $('.tb-check-single').iCheck(iCheckboxOpt);

        },
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    new $.fn.dataTable.FixedHeader( table ).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt); 
     })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


function changeActive(courseId,userId){
   //alert($('#'+courseId+userId).val());
  // alert(courseId);
  // alert(userId);

  bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
            $.ajax({
                  url: siteUrl+'/admin/member/change_active',
                  type: 'POST',
                  dataType: 'json',
                  data: {
                        csrfToken: csrfToken,
                        courseId : courseId ,
                        userId : userId ,
                        manage_val : $('#'+courseId+userId).val()
                   },
            })
            .done(function(data) {
                console.log(data);
                  if(data.manage_status == 1)
                  {
                        $('#'+courseId+userId).val(data.manage_val);
                        //$('#data-list').DataTable().ajax.reload(); 

                  }
                  else if(data.manage_status == 0)
                  {
                        $('#'+courseId+userId).val(data.manage_val);
                        //$('#data-list').DataTable().ajax.reload(); 
                     
                        
                  }
            })
            .fail(function() {
            
                         
            });

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
          "callback": function() {
            $('#data-list').DataTable().ajax.reload(); 
          }
        }
      }
    });
    
}