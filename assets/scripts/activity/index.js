$(document).ready(function() {
    var event = siteUrl+'activity/calendar';
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      //defaultDate: '2018-03-12',
      //navLinks: true, // can click day/week names to navigate views
      editable: false,
      //eventLimit: true, // allow "more" link when too many events
      eventMouseover: function (data, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'หัวข้อ ' + ': ' + data.title + '<br>วัน : '+data.date+'<br>เวลา : '+data.time+'<br>สถานที่ : '+data.location+'</div>';


            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });


        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();

        },
        dayClick: function () {
            tooltip.hide()
        },
        eventResizeStart: function () {
            tooltip.hide()
        },
        eventDragStart: function () {
            tooltip.hide()
        },
        viewDisplay: function () {
            tooltip.hide()
        },
      lang: 'th',
      events: event,
        eventClick: function(event) {
          if (event.url) {
            window.location(event.url);
            return false;
          }
        }
    });

  });